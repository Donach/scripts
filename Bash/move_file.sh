#!/bin/bash
##############################################################################
# Module Name:  move_file.sh                                                 #
# Author     :  Daniel Havlicek                                              #
# Date       :  18.03.2022                                                   #
#                                                                            #
# Description                                                                #
# This script :  Move target file                                            #
#                                                                            #
# Parameters:                                                                #
# 1.  Source Full Path                                                       #
# 2.  Target Full Path                                                       #
# 3.  Project Folder name                                                    #
#                                                                            #
#                                                                            #
# Change History                                                             #
# --------------                                                             #
#                                                                            #
# Version Date        Author           Comments                              #
# 1.0     18.03.2022  Daniel Havlicek  Original version                      #
#                                                                            #
##############################################################################
#
#script call>                          $0            $1           $2        $3
#$PMRootDir/scripts/$PMFolderName/move_file.sh  source_path target_path $PMFolderName

# write passed parameters out to log file
echo "Parameter dump " 
echo "Script Name_______________ $0"
echo "Source Full Path__________ $1"
echo "Target Full Path__________ $2"
echo "FolderName________________ $3"

# Assign path for the files
v_srcpath=$1
v_tgtpath=$2

if [[ -f $v_srcpath && ! -f $_tgtpath && "$v_srcpath" != "" && "$v_srcpath" =~ "$3" && "$v_tgtpath" =~ "$3" && "$3" != "" ]] ; then
    echo "Moving file from: $v_srcpath  --> to -->  $v_tgtpath"
	mv $v_srcpath $v_tgtpath
    exit 0
else
    echo "Wrong path, or target file exists!"
    exit 1
fi
