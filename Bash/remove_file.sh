#!/bin/bash
##############################################################################
# Module Name:  remove_file.sh                                               #
# Author     :  Daniel Havlicek                                              #
# Date       :  18.03.2022                                                   #
#                                                                            #
# Description                                                                #
# This script :  Removes target file                                         #
#                                                                            #
# Parameters:                                                                #
# 1.  Source file path                                                       #
# 2.  Source file name                                                       #
# 3.  Project Folder name                                                    #
#                                                                            #
#                                                                            #
# Change History                                                             #
# --------------                                                             #
#                                                                            #
# Version Date        Author           Comments                              #
# 1.0     18.03.2022  Daniel Havlicek  Original version                      #
#                                                                            #
##############################################################################
#
#script call>                          $0             $1           $2        $3
#$PMRootDir/scripts/$PMFolderName/remove_file.sh  source_path source_file $PMFolderName

# write passed parameters out to log file
echo "Parameter dump " 
echo "Script Name_______________ $0"
echo "Source file path__________ $1"
echo "Source file name__________ $2"
echo "FolderName________________ $3"

# Assign path for the files
v_srcpath=$1
v_srcfile=$2

if [[ -f $v_srcpath/$v_srcfile && "$v_srcpath" != "" && "$v_srcpath" != "/" && "$v_srcpath" =~ "$3" && "$3" != "" ]] ; then
    echo "Removing file with '-rf': $v_srcpath/$v_srcfile"
	rm -rf $v_srcpath/$v_srcfile
    exit 0
else
    echo "File not found or invalid path given: $v_srcpath/$v_srcfile"
    exit 1
fi
