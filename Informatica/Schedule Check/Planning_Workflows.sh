#!/bin/bash

. ~/.profile

#/opt/Informatica/infa_shared/scripts/PIRBN_00_CSHMN_00/Planning_Workflows.sh $PMIntegrationServiceName $PMDomainName /opt/Informatica/infa_shared/scripts/PIRBN_00_CSHMN_00/Input_PIRBN_00_CSHMN_00.csv /opt/Informatica/infa_shared/WorkflowLogs/PIRBN_00_CSHMN_00 Y
#sh -c "/opt/Informatica/infa_shared/scripts/PSF91/Planning_Workflows.sh devis $PMDomainName /opt/Informatica/infa_shared/TgtFiles/PSF91/wf_Schedule_CHECK/wf_to_be_scheduled.csv /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK N"

Infa_IS=$1
Infa_Domain=$2
Input_CSV=$3
Log_Path=$4
Flag_RunMode=$5 # S - schedule; U - unschedule; L - list; T - trigger

OutputResult=$Input_CSV".result"


currDate=$(date +%Y%m%d_%H%M%S)
LOG_FILE="$Log_Path/Planning_Workflows_"$currDate"_$Flag_RunMode.log"

#Debugs
#echo "$LOG_FILE" >> $LOG_FILE
#echo "$LOG_FILE" >> /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK/test.txt
#chmod 777 /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK/test.txt
touch $LOG_FILE
if [[ $Infa_Domain == "devdom" ]]; then
	chmod -R 777 $LOG_FILE
	chmod -R 777 $Log_Path
fi

#All outputs are redirect to the logfile
exec >> $LOG_FILE

# debugg - all input parametters
if [[ $Infa_Domain == "devdom" ]]; then echo -e "Run Params:\n$@\n\n"; fi

#create arrays from CSV files with comma separator
arrFolder=( $(cut -d ';' -f1 $Input_CSV ) )
arrWorkflow=( $(cut -d ';' -f2 $Input_CSV ) )
arrCL_User=( $(cut -d ';' -f3 $Input_CSV ) )
arrEnv_Pass=( $(cut -d ';' -f4 $Input_CSV ) )
passEnv=""
pmrunmode=""
pmdetails=""
procFolders=""
errCount=0
okCount=0

# If RunMode is "L" run check to list all scheduled workflows, else run un/schedule procedure
case "$Flag_RunMode" in
	"L")
		pmrunmode="getservicedetails"
		;;
	"LA")
		pmrunmode="getservicedetails"
		;;
	"U")
		pmrunmode="unscheduleworkflow"
		;;
	"S")
		pmrunmode="scheduleworkflow"
		;;
	"T")
		pmrunmode="startworkflow"
		;;
	"STOP")
		pmrunmode="stopworkflow"
		;;
	*)
		pmrunmode=""
		;;
esac
if [[ $pmrunmode == "" ]]; then
	echo "RunMode is not properly defined! (5th parameter)"
	echo "Terminating..."
	exit 1
fi
if [[ -f $OutputResult ]]; then
	rm $OutputResult
fi

# Do for all array elements
for (( index = 0; index < ${#arrFolder[@]}; index++ )) # TODO: Find out how this works exactly
do 
	folder="${arrFolder[$index]}"
	userName="${arrCL_User[$index]}"
	passEnv="${arrEnv_Pass[$index]}"
	workflow="${arrWorkflow[$index]}"
	echo -e "\nProcessing $folder - Worfklow $workflow"
	# Check for RunMode, if its just to "L"ist scheduled workflows, we want to lookup the schedule for each folder only once.
	if [[ "$Flag_RunMode" =~  "L" && $procFolders != *"$folder"* ]]; then  
		procFolders="$procFolders;$folder"
		if [[ "$Flag_RunMode" ==  "L" ]]; then
			pmdetails="-scheduled | grep 'Workflow:' | cut -d'[' -f 2 | cut -d']' -f 1" 
		else
			pmdetails="-all" 
		fi
	elif [[ ! "$Flag_RunMode" =~  "L" ]]; then
		pmdetails="-f $folder $workflow"
	else # If RunMode is "L" but the current folder was already processed, skip to next array member
		continue
	fi
	# Build the PMCMD command using common params and specific pmdetails based on RunMode
	pmcommand="pmcmd $pmrunmode -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails"

	# Triggering only - check if the workflow is currently running
	if [[ "$Flag_RunMode" == "T" || "$Flag_RunMode" == "STOP" ]]; then
		wfCurrentStatus=$( pmcmd getworkflowdetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails | grep 'Workflow run status' | cut -d '[' -f2 | cut -d ']' -f1 )
		echo "Current workflow status: $wfCurrentStatus"
		if [[ "$wfCurrentStatus" == "Running" ]]; then
			if [[ "$Flag_RunMode" == "T" ]]; then
				echo "Cannot start workflow $workflow, it is currently running!"
				echo "$folder - $workflow - ERROR - Workflow is running" >> $OutputResult
				(( errCount += 1 ))
				continue
			else
				echo "Workflow is running, going to stop it now..."
			fi
		else
			if [[ "$Flag_RunMode" == "T" ]]; then
				echo "Workflow is not running, going to start it now..."
			else
				echo "Workflow is not running, no need to stop it..."
				continue
			fi
		fi
	fi

	## - This might be necessary, keep this code around!
	#export $passEnv # TODO: Has to be run to load ENV variable properly ?

	# Run the actual PMCMD command and save its output to variable. Save also its result code for later check
	resultCode=0
	result=$(eval $pmcommand; resultCode=$?;) # dynamically generated command based on RunMode flag

	# Based on RunMode, process the result from previous command if the result code is 0
	if [[ $resultCode != 0 || $result =~ "ERROR" ]]; then
		echo "ERROR: !!!!script The process has failed!!!!! "
		echo "Code: $resultCode"
		echo "Domain: $Infa_Domain; Service: $Infa_IS"
		echo "Could not $pmrunmode ${arrWorkflow[$index]} in folder ${arrFolder[$index]} using ${arrCL_User[$index]} as user and $passEnv and name of ENV password."
		(( errCount += 1 ))
		echo "Result: $result"
		# exit 1
	else
		if [[ "$Flag_RunMode" ==  "L" ]]; then
			# Take the results and format them so they will be read as lines by the while loop, prefixing each workflow with its foldername and putting it to temporary list of scheduled workflows
			echo $result | cut -f 1 | tr ' ' '\n'  | while read line; do echo "$folder;$line;" >> $OutputResult; done
			if [[ ! -f $OutputResult ]]; then
				echo "ERROR: Could not create file with list of scheduled workflows! - $OutputResult"
				resultCode=255
			else
				echo "SUCCESS: List of scheduled workflows was created: $OutputResult"
			fi
		elif [[ "$Flag_RunMode" ==  "LA" ]]; then
			echo $result
		else
			# INFO
			checkInfo=$(echo $result | grep $workflow | grep 'INFO')
			#echo "DEBUG INFO:"$checkInfo
			# ERROR
			checkError=$(echo $result | grep $workflow | grep 'ERROR')
			#echo "DEBUG ERROR:"$checkError
			if [[ $checkError == "" ]]; then
				echo "$folder - $workflow ---> OK" >> $OutputResult
				(( okCount += 1 ))
			else
				echo "$folder - $workflow ---> ERROR" >> $OutputResult
				(( errCount += 1 ))
			fi

			echo "Results: $result"  | grep $workflow
		fi

	fi
done


echo -e "\n\nThe process has finished successfully"
echo "Successfully scheduled workflows: $okCount"
echo "Total number of errors: $errCount"

echo -e "\n\nSuccessfully scheduled workflows: $okCount" >> $OutputResult
echo -e "Total number of errors: $errCount" >> $OutputResult
echo "Exit code: "$resultCode

exit $resultCode
