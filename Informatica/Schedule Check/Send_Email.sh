recipients=$1
subject=$2
emailBodyFile=$3
attachment=$4 # Y - Take last log of Planning_Workflows
dynamicSubject=$5 # Y - to use predefined functionality adding results from file into subject

if [[ $dynamicSubject == "Y" ]]; then 
	if [[ $subject == *'NOT re-scheduled'* ]]; then
		subject=$subject"Errors: $(cat $emailBodyFile | grep 'Total number of errors' | cut -d ':' -f2)"
	elif [[ $subject == *'re-scheduled'* ]]; then
		subject="$subject$(cat $emailBodyFile | grep 'Successfully scheduled workflows' | cut -d ':' -f2), Errors: $(cat $emailBodyFile | grep 'Total number of errors' | cut -d ':' -f2)"
	fi
fi
if [[ ! -f $attachment ]]; then filename=$(ls -ltr $attachment | grep 'Planning_Workflows' | tail -1 | cut -d':' -f 2 | cut -d' ' -f 2); attachment="$attachment/$filename"; fi

# Send Email
cat $emailBodyFile | mail -s "$subject" -S from=infa -a "$attachment" "$recipients"

exit $?