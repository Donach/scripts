'''
   Name:            IPC_Simple_Checker.py
   Description:     This script processes all Workflows/Worklets, Mappings/Mapplets and Transformtaions, checking for naming, correct logging,... 
                    Rewrite of original script IPC_Simple_Checker.pl written in Perl.
   Author:          Daniel Havlicek
   Date:            01/05/2018
   Changed:         21/06/2018 - Daniel Havlicek
   Changelog:       21/06/2018 - Daniel Havlicek					
                     - Added script description header					
                     - Fixed logging of Mappings and Transformations using wrong variable

Processing Order:
- Initialization - create base file for each object type
- Process each XML - each XML in the INPUT dir is processed
- Process XML on folder-basis - Since IPC XML is organized into folders, process each folder-basis
- Process all Workflows/Worklets, Mappings/Mapplets and Transformation in each folder
	- Process Workflow/Worklet:
		Workflow Only:
		- Process all links between tasks/sessions/commands in the workflow
		- Process all workflow attributes
		- Process parameter file (if there is any)
		- Check for log file directory settings
		Workflow And Worklet:
		- Process all TASKINSTANCE references to all Workflow/Worklet elements
		- Process TASKs
		Workflow Only:
		- Process Schedulers
	- Process Mapping/Mapplet
		- Process each Mapping/Mapplet Transformation (Same process as below)
	- Process Transformation
		- Process if Transformation is Expression
		- Process if Transformation is Joiner
- Finalize all XMLs

'''

import xml.etree.ElementTree as ET
import sys # Imports package for handling arguments
import os #Creation of directories, other system access
import re # Regular Expressions
from pathlib import Path


xmlPath = Path('C:\\_Work\\CheckerScript\\02_Windows\\INPUT\\FLECS_00_RESTA_00\\FLECS_00_RESTA_00.xml')
severityInfo    = "INFO"
severityWarning = "WARNING"
severityError   = "ERROR"
severityFatal   = "FATAL"
ENCODING        = "utf-8"
# TEMPORARY
if (len(sys.argv) == 1):
    sys.argv.append("C:\\_Work\\CheckerScript\\Python\\INPUT\\")
    sys.argv.append("C:\\_Work\\CheckerScript\\Python\\OUTPUT\\")
    sys.argv.append("C:\\_Work\\CheckerScript\\Python\\PARAM\\")
    
# END TEMPORARY

print("Arguments: OK") if len(sys.argv) == 4 else sys.exit("Wrong number of arguments, expecting 3 - INPUTFOLDER, OUTPUTFOLDER, PARAMFOLDER.")
# Initialize variables
inputDirectory  = Path(sys.argv[1])
outputDirectory = Path(sys.argv[2])
paramDirectory  = Path(sys.argv[3])

directorySeparator = os.pathsep

wRows = 0
sRows = 0
tRows = 0
mRows = 0

xmlParserRoot = None
wfFileName = ""
sFileName = ""
tFileName = ""
mFileName = ""
stream_wfFileName = ""
stream_sFileName = ""
stream_tFileName = ""
stream_mFileName = ""

parameters = {} # Parameter hashmap
fileCounter = 0
# END Initialize variables

if (not (inputDirectory.is_dir() and outputDirectory.is_dir() and paramDirectory.is_dir())):
    print("Creating missing directories...")
    os.makedirs(inputDirectory)
    os.makedirs(outputDirectory)
    os.makedirs(paramDirectory)



def processInput(paramInputDir):
    """ Main processing function. Runs all other necessary functions. \n
    Takes: string paramInputDir \n
    "paramInputDir" should be full path to the INPUT directory on the file system.

    """
    inputDir = Path(paramInputDir) # Full path of currently processed folder
    basename = os.path.basename(inputDir) # Name of currently processed folder
    global fileCounter
    global wfFileName
    global sFileName
    global tFileName
    global mFileName
    global stream_wfFileName
    global stream_sFileName
    global stream_tFileName
    global stream_mFileName
    global wRows
    global sRows
    global tRows
    global mRows
    global outputDirectory
    '''
    # counter of rows in the compliance check report file for workflows
    wRows = 0
    # counter of rows in the compliance check report file for sessions
    sRows = 0
    # counter of rows in the compliance check report file for tasks
    tRows = 0
    # counter of rows in the compliance check report file for mappings
    mRows = 0
    '''
    wfFolderName = outputDirectory / basename #(basename  + "_Implementation_Workflows.xml")
    #print(wfFolderName)
    wfFolderName.exists() or wfFolderName.mkdir() #sys.exit("Cannot open file "+wfFileName)

    # create file for workflow compliance check report
    wfFileName = wfFolderName / (basename  + "_Implementation_Workflows.xml")
    #print("\tFile: "+str(wfFileName))
    wfFileName.exists() or wfFileName.touch() #sys.exit("Cannot open file "+wfFileName)
    stream_wfFileName = open(wfFileName, 'w')
    # initialization of XML in special Excel format
    initializeXMLFile(stream_wfFileName, "Workflow")
    #logToReport(severity, folderName, workflowName, sessionName, taskName, mappingName, transformationName, issueDescription, expected, found)
    logToReport("Severity", "Folder Name", "Workflow Name",   "","","","",      "Issue Description", "Expected Value", "Found Value")

    # create file for sessions compliance check report 
    sFileName = wfFolderName / (basename + "_Implementation_Sessions.xml")
    sFileName.exists() or sFileName.touch()
    stream_sFileName = open(sFileName, 'w')
    # initialization of XML in special Excel format
    initializeXMLFile(stream_sFileName, "Session")
    logToReport("Severity", "Folder Name", "Workflow Name","Session Name",  "","","",      "Issue Description", "Expected Value", "Found Value")

    # create file for tasks compliance check report 
    tFileName = wfFolderName / (basename + "_Implementation_Tasks.xml")
    tFileName.exists() or tFileName.touch()
    stream_tFileName = open(tFileName, 'w')
    # initialization of XML in special Excel format
    initializeXMLFile(stream_tFileName, "Task")
    # log header into the tasks compliance check report
    logToReport("Severity", "Folder Name", "Workflow Name","",  "Task Name","","",      "Issue Description", "Expected Value", "Found Value")

    # create file for mapping compliance check report 
    mFileName = wfFolderName / (basename + "_Implementation_Mappings.xml")
    mFileName.exists() or mFileName.touch()
    stream_mFileName = open(mFileName, 'w')
    # initialization of XML in special Excel format
    initializeXMLFile(stream_mFileName, "Mapping")
    # log header into the mapping compliance check report
    logToReport("Severity", "Folder Name",      "","","",   "Mapping Name","Transformation Name", "Issue Description", "Expected Value", "Found Value")


    for (dirpath, dirnames, filenames) in (os.walk(inputDir)):
        for localFile in filenames:
            
            if ( (localFile == re.match(r'^\.$', localFile) or (localFile == re.match(r'^\.\.$', localFile) ) ) ):
                continue
            
            if (Path(localFile).exists):
                fileCounter += 1
                print("\t File ("+str(fileCounter)+"): "+localFile)
                # Process the XML file - need to get full path
                processFile(dirpath+os.sep+localFile)

    finalizeXMLFile(wfFileName, wRows)

    finalizeXMLFile(mFileName, mRows)

    finalizeXMLFile(tFileName, tRows)

    finalizeXMLFile(sFileName, sRows)

    if (wRows < 2): print("Empty report Workflow file: "   +wfFileName.name)
    if (mRows < 2): print("Empty report Mapping file: "    +mFileName.name)
    if (tRows < 2): print("Empty report Task file: "       +tFileName.name)
    if (sRows < 2): print("Empty report Session file: "    +sFileName.name)
    

def processFile(inputFile):
    """Go through each POWERMART and each Repository inside it, and submit all Folders for processing. \n
    Takes: string inputFile (path to a file on filesystem)
    """
    global xmlParserRoot
    xmlParser = ET.parse(inputFile)
    xmlParserRoot = xmlParser.getroot()
    #print(xmlParserRoot.tag)
    # 
    for POWERMART in xmlParser.iter('POWERMART'):
        print(2*"\t"+str(POWERMART.tag))
        for REPOSITORY in POWERMART.iter('REPOSITORY'):
            print(3*"\t"+str(REPOSITORY.tag)+": "+REPOSITORY.attrib.get('NAME'))
            #print(REPOSITORY.attrib.get('NAME'))
            for FOLDER in REPOSITORY.iter('FOLDER'):
                print(4*"\t"+FOLDER.tag+": "+FOLDER.attrib.get('NAME'))
                #print(FOLDER.attrib.get('NAME'))
                processFolder(FOLDER)

def processFolder(inputFolder):
    """ Processing of Repository Folder. Function goes through each Workflow/Worklet, Mapping/Mapplet and 
    Reusable Transformation in the supplied folder and submits them for furthe processing.\n
    Takes: XMLElement inputFolder 
    """
    folderName = inputFolder.attrib.get('NAME')
    workflows   =   inputFolder.findall('WORKFLOW')
    workflows   +=  inputFolder.findall('WORKLET')
    mappings    =   inputFolder.findall('MAPPING')
    mappings    +=  inputFolder.findall('MAPPLET')
    reuseTrans  =   inputFolder.findall('TRANSFORMATION')

    for workflow in workflows:
        print(5*"\t"+workflow.tag+": "+workflow.attrib.get('NAME'))
        processWorkflow(folderName, workflow)
    for mapping in mappings:
        print(5*"\t"+mapping.tag+": "+mapping.attrib.get('NAME'))
        processMapping(folderName, mapping)
    for trans in reuseTrans:
        #print(5*"\t"+trans.tag+": "+trans.attrib.get('NAME'))
        processTransformation(folderName, "", trans) # Reusable Transformation has no single mapping it would be bound to


# Processing for Workflows and Worklets
def processWorkflow(folderName, workflowElement):
    """ Processing of Workflow and Worklet objects.\n
    Takes: string folderName, XMLElement workflowElement
    """
    global severityFatal
    global severityError
    global severityInfo
    global severityWarning
    workflowName = workflowElement.attrib.get('NAME')
    #folderName = folderPath.basename
    print(6*"\t"+workflowElement.tag+": "+workflowName)
    print(6*"\t"+workflowElement.tag+": "+workflowElement.attrib.get('VERSIONNUMBER'))
    # WORKFLOW and WORKLET is processed by one function, however still some things are relevant only for WORKFLOW
	# may be split to multiple function call for better readability

    # Run specific function for Workflow only
    if (workflowElement.tag != 'WORKLET'):
        processWorkflowOnly(folderName, workflowElement)
        
    # For all objects to be present in workflow, TASKINSTANCE element should exist as a child of WORKFLOW element
	# taskinstance - reference to definition of TASK/SESSION    
    processInstances(folderName, workflowElement)

    # Process all tasks in the workflow, reusable task definitions as well
    processTasks(folderName, workflowElement)

    # Process all scheduler details
    processScheduler(folderName, workflowElement)



def processWorkflowOnly(folderName, workflowElement):
    """ Process WORKFLOW object with specific conditions applied. \n
    Takes: string folderName, XMLElement workflowElement
    """
    global severityFatal
    global severityError
    global severityInfo
    global severityWarning
    global parameters
    global paramDirectory
    workflowName = workflowElement.attrib.get('NAME')
    #folderName = folderPath.basename
    # read integration service name
    integrationService = workflowElement.attrib.get('SERVERNAME')

    # it is possible that workflow has no integration service set, it has to be checked whether DEFINED
    if (not integrationService): # Check if Integration Service is correct
        # Currently all usages of "x" services have to be allowed via exceptions
        # IMPORTANT: to run on 10.2 the "ng" on next line should be removed from the checks on the new servers
        if(not re.match(r'^ng(dev|tst|acc|prd)is$', integrationService, flags=re.I)):
            if (not (((re.match(r'DWHDQ_11_NNBNK_00', folderName) ) #exception approved by Adrie on 14.3.2018, IPS-5038, 4.4.2018 IPS-5128, 15.5.2018 IPS-5313
                    # and # exception on whole folder DWHDQ_11_NNBNK_00 approved by Adrie on 16.5.2018 - by email 
                    # (
                    #    $workflowName =~ /wf_MIFID_VERRIJKING/i
                    # or $workflowName =~ /wf_Run_Polisdata_CV_Labora_Euroverzekeringen/i
                    # or $workflowName =~ /s_MP_House_STG_hF_PS_TEAM_TOEKENNING/i
                    # or $workflowName =~ /wf_Run_House_Werkorder/i
                    # or $workflowName =~ /wf_Run_House_Dossier/i
                    # or $workflowName =~ /wf_House_test_processtap/i
                    # or $workflowName =~ /wf_Run_House_Historische_Datalaag/i
                    # or $workflowName =~ /wf_MARINA/i						
                    # or $workflowName =~ /wf_BPM/i
                    # or $workflowName =~ /wf_Staging_OneTool/i
                    # or $workflowName =~ /wf_HIST_AUDITTRAILBPM/i
                    # ) 
            ) or (re.match(r'SAPHN_00_CUDAP_00', folderName) and re.match(r'wf_cudap_data_load', workflowName, flags=re.I)) ) ):
                # IMPORTANT: to run on 10.2 remove "ng"
                logToReport(severityFatal,
                            folderName,
                            workflowName, 
                            "",
                            "",
                            "",
                            "",
                            "Wrong integration service name used.",
                            "ngdevis or ngtstis or ngaccis or ngprdis",
                            integrationService)
    else: # else if integration service is not set at all
        # IMPORTANT: to run on 10.2 remove "ng"
        logToReport(severityFatal,
                    folderName,
                    workflowName, 
                    "",
                    "",
                    "",
                    "",
                    "Integration service name not set.",
                    "ngdevis or ngtstis or ngaccis or ngprdis",
                    "none")

    # process all links between all tasks/sessions/commands in the workflow           
    workflowLinks = workflowElement.findall('WROKFLOWLINK')
    for workflowLink in workflowLinks:
        condition = workflowLink.attrib.get('CONDITION')
        fromTask = workflowLink.attrib.get('FROMTASK')
        toTask = workflowLink.attrib.get('TOTASK')

        # make it readable, so do substitution for special characters that are not directly allowed in XML
        condition = replaceXMLChars(condition)

        # If Condition is empty and fromTask is NOT equal to START, run logToReport
        if (re.match(r'^\s*\t*$', condition)): 
            if (fromTask != 'START'):
                logToReport(severityWarning,
                            folderName,
                            workflowName,
                            "",
                            "",
                            "",
                            "",
                            "Workflow link from " + fromTask + " to " + toTask + " is empty.",
                            "Previous task Succeeded/ Failed check",
                            "empty")
        elif (not (re.match(str(r'^\$'+fromTask+r'\.Status\s*\!*=\s*SUCCEEDED$'), condition, flags=re.I)
            or re.match(str(r'^\$'+fromTask+r'\.PrevTaskStatus\s*\!*=\s*SUCCEEDED$'), condition, flags=re.I)
            or re.match(str(r'^\$'+fromTask+r'\.Status\s*\!*=\s*FAILED$'), condition, flags=re.I)
            or re.match(str(r'^\$'+fromTask+r'\.PrevTaskStatus\s*\!*=\s*FAILED$'), condition, flags=re.I)
            or re.match(str(r'^\$'+fromTask+r'\.Condition\s*\!*=\s*TRUE$'), condition, flags=re.I)
            or re.match(str(r'^\$'+fromTask+r'\.Condition\s*\!*=\s*FALSE$'), condition, flags=re.I))):
                print(condition)
                logToReport(severityWarning,
                            folderName,
                            workflowName,
                            "",
                            "",
                            "",
                            "",
                            "Complex workflow link condition between " + fromTask + " and " + toTask + " found. Should be put into the separate decision task.",
                            "Succeeded/ Failed/ True/ False",
                            condition)

    # workflow specific atributes are stored in ATTRIBUTE elements
    attributes = workflowElement.findall('ATTRIBUTE')
    # to be usable I need to first make hashmap where value of NAME attribute is key and VALUE attribute is value of hashmap
    workflowAttributes = {}
    for attribute in attributes:
        workflowAttributes.update({attribute.attrib.get('NAME') : attribute.attrib.get('VALUE')})
    #print for testing purposes
    #for key in workflowAttributes:
    #	print (key + ": " + workflowAttributes.get(key))

    # Check if the log filename matches the name of workflow
    if (workflowAttributes.get('Workflow Log File Name') != ("" + workflowName + ".log")
        and workflowAttributes.get('Workflow Log File Name') != ('\\$PMWorkflowName.log')):
        logToReport(severityFatal,
                    folderName,
                    workflowName,
                    "",
                    "",
                    "",
                    "",
                    "Workflow log name and workflow name do not match.",
                    workflowName +".log",
                    workflowAttributes.get('Workflow Log FIle Name'))
    
    # Check if Workflow logs retention is set correctly
    if (workflowAttributes.get('Save Workflow log by') == 'By runs'):
        logToReport(severityWarning,
                    folderName,
                    workflowName,
                    "","","","",
                    "Workflow logs saved by runs. As a feature of informatica only last log will be accessible via Monitor. Older logs will be available only on file system.",
                    "by timestamp",
                    "by runs")
        if (workflowAttributes.get('Save workflow log for these runs') == "0"):
            logToReport(severityFatal,
                        folderName,
                        workflowName,
                        "","","","",
                        "Only last version of the workflow log file is stored.",
                        ">10",
                        "0")
        elif (int(workflowAttributes.get('Save workflow log for these runs')) < 10):
            logToReport(severityFatal,
                        folderName,
                        workflowName,
                        "","","","",
                        "Increase the amount of the workflow logs stored.",
                        ">10",
                        workflowAttributes.get('Save workflow log for these runs'))

    parameters = {}

    paramFileName = workflowAttributes.get('Parameter Filename')
    find = r'&#x5c;'
    paramFileName = re.sub(str(find), r'\/', paramFileName)
    paramFileName = re.sub(r'\$PMRootDir\/parmfile', str(paramDirectory), paramFileName, flags=re.I)
    paramFileName = re.sub(r'\/opt\/Informatica\/infa_shared\/parmfile', str(paramDirectory), paramFileName, flags=re.I)
    # replace linux directory separator with OS separator 
    paramFileName = re.sub(r'\/', str("\\"+os.sep), paramFileName)
    # replace windows directory separator with OS separator 
    paramFileName = re.sub(r'\\', str("\\"+os.sep), paramFileName)

    if (paramFileName != ""):
        # check whether parameter file is present, if not it should point to the situation that the parameter file has not been part of the SVN package
        if (not (Path(paramFileName).exists)):
            logToReport(severityFatal,
                        folderName,
                        workflowName,
                        "","","","",
                        "Workflow parameter file is not listed in the parmfile/file_list.cfg in SVN",
                        workflowAttributes.get('Parameter Filename'),
                        "Not found")
            parameters = {}
        else:
            readParameterFile(paramFileName)

    # Replace all slashes in the WF Log File Directory path for forward slash "/"
    #workflowAttributes.update({'Workflow Log File Directory' : re.sub(str(find), r'\/', workflowAttributes.get('Workflow Log File Directory'))})

    # Check what is the workflow file log directory, it should not be set to the Infa default one
    if (workflowAttributes.get('Workflow Log File Directory') == "$PMWorkflowLogDir"
        or re.match(r'^\$PMWorkflowLogDir.$', workflowAttributes.get('Workflow Log File Directory'))):
        if (parameters.get('$PMWorkflowLogDir')): # If parameters PMWorkflowLogDir is defined
            #print(parameters.get('$PMWorkflowLogDir'))
            if (not (re.match(str(r"\/opt\/Informatica\/infa_shared\/WorkflowLogs\/"+folderName+r"\/"), parameters.get('$PMWorkflowLogDir'), flags=re.I)
                    or re.match(str(r"\$PMRootDir\/WorkflowLogs\/"+folderName), parameters.get('$PMWorkflowLogDir'), flags=re.I))
                ):
                logToReport(severityFatal,
                            folderName,
                            workflowName,
                            "","","","",
                            "Workflow Log File Directory is improperly set in param file.",
                            "$PMRootDir/WorkflowLogs/"+folderName+"/",
                            parameters.get('$PMWorkflowLogDir'))
        else: # Else if parameters PMWorkflowLogDir is NOT defined
            logToReport(severityFatal,
                            folderName,
                            workflowName,
                            "","","","",
                            "Workflow Log File Directory is improperly set.",
                            "$PMRootDir/"+folderName+"/",
                            workflowAttributes.get('Workflow Log File Directory'))
    elif (not (re.match(str(r'\$PMWorkflowLogDir\/'+folderName), workflowAttributes.get('Workflow Log File Directory'), flags=re.I)
                or re.match(str(r'\/opt\/Informatica\/infa_shared\/WorkflowLogs\/'+folderName+r'\/'), workflowAttributes.get('Workflow Log File Directory'), flags=re.I)
                or workflowAttributes.get('Workflow Log File Directory') == "$PMWorkflowLogDir/$PMFolderName/")
        ):
        logToReport(severityFatal,
                    folderName,
                    workflowName,
                    "","","","",
                    "Workflow Log File Directory is improperly set.",
                    "$PMRootDir/"+folderName+"/",
                    workflowAttributes.get('Workflow Log File Directory'))
    
    if (workflowAttributes.get('Parameter Filename') != ""
        and not(re.match(str(r'\$PMROOTDIR\/parmfile\/'+folderName+r'\/.*\.prm'), workflowAttributes.get('Parameter Filename'), flags=re.I )
                or re.match(str(r'\/opt\/Informatica\/infa_shared\/parmfile\/'+folderName+r'\/.*\.prm'), workflowAttributes.get('Parameter Filename'), flags=re.I ))
        ):
        logToReport(severityFatal,
                    folderName,
                    workflowName,
                    "","","","",
                    "Workflow parameter file name set improperly.",
                    "$PMROOTDIR/parmfile/"+folderName+"/.*.prm",
                    workflowAttributes.get('Parameter Filename'))

def processInstances(folderName, workflowElement):
    """ Process TASKINSTANCE object with specific conditions applied. \n
    Takes: string folderName, XMLElement workflowElement
    """
    global severityFatal
    global severityError
    global severityInfo
    global severityWarning
    workflowName = workflowElement.attrib.get('NAME')
    # For all objects to be present in workflow, TASKINSTANCE element should exist as a child of WORKFLOW element
	# taskinstance - reference to definition of TASK/SESSION
    instances = workflowElement.findall('TASKINSTANCE')
    for instance in instances:
        instanceType = instance.attrib.get('TASKTYPE')
        instanceName = instance.attrib.get('NAME')
        if (instanceType == "START"):
            continue
        if (instanceType == "SESSION"):
            # session's specifics are verified in different procedure
            processSession(folderName, workflowElement, instance)
        
        if (instance.attrib.get('FAIL_PARENT_IF_INSTANCE_FAILS') == "YES"):
            issueDescr = ""
            taskName = ""
            sessionName = ""
            if (instanceType == "SESSION"): 
                issueDescr = "Workflow will not fail when this session fails. Only good exceptions are allowed."
                sessionName = instanceName
            else:
                issueDescr = "Workflow will not fail when this task fails. Only good exceptions are allowed."
                taskName = instanceName
            logToReport(severityError,
                        folderName,
                        workflowName,
                        sessionName,
                        taskName,
                        "","",
                        issueDescr,
                        "YES",
                        instance.attrib.get('FAIL_PARENT_IF_INSTANCE_FAILS'))

        if (instance.attrib.get('ISENABLED') == "YES"):
            issueDescr = ""
            taskName = ""
            sessionName = ""
            if (instanceType == "SESSION"):
                issueDescr = "All sessions have to be enabled!"
                sessionName = instanceName
            else:
                issueDescr = "All tasks have to be enabled!"
                taskName = instanceName
            logToReport(severityError,
                        folderName,
                        workflowName,
                        sessionName,
                        taskName,
                        "","",
                        issueDescr,
                        "YES",
                        instance.attrib.get('ISENABLED'))

        if (instance.attrib.get('TREAT_INPUTLINK_AS_AND') == "YES"):
            issueDescr = ""
            taskName = ""
            sessionName = ""
            if (instanceType == "SESSION"):
                issueDescr = "Please check why OR is used instead of AND for input links. Potential error."
                sessionName = instanceName
            else:
                issueDescr = "Please check why OR is used instead of AND for input links. Potential error."
                taskName = instanceName
            logToReport(severityWarning,
                        folderName,
                        workflowName,
                        sessionName,
                        taskName,
                        "","",
                        issueDescr,
                        "AND",
                        "OR")


def processScheduler(folderName, workflowElement):
    """ Process Scheduler, checking for scheduling settings - Recurrence, Run on server init,...\n
    Takes: string folderName, XMLElement workflowElement
    """
    global severityFatal
    global severityError
    global severityInfo
    global severityWarning
    global xmlParserRoot
    workflowName = workflowElement.attrib.get('NAME')
    schedulers = workflowElement.findall('SCHEDULER')
    if (workflowElement.tag != "WORKLET"):
        if (workflowElement.attrib.get('REUSABLE_SCHEDULER') == "NO"):
            pass #already defined
        else:
            schedulers = xmlParserRoot.findall('.//SCHEDULER[@REUSABLE="YES" and @NAME="' + workflowElement.attrib.get('SCHEDULERNAME') + '"]')

    for schedulerNotReusable in schedulers:
        scheduleInfo = schedulerNotReusable.find('SCHEDULEINFO')
        if (scheduleInfo):
            scheduleType = scheduleInfo.attrib.get('SCHEDULETYPE')
            endoptions = scheduleInfo.find('ENDOPTIONS')
            if (scheduleType and scheduleType == 'ONSERVERINIT'):
                logToReport(severityFatal,
                            folderName,
                            workflowName,
                            "","","","",
                            "Workflow is scheduled to start after the integration service initialization. Option run on integration service initialization is set inside the scheduler.",
                            "not checked",
                            "checked" )
            elif (scheduleType and scheduleType == 'CONTINUOUS'):
                logToReport(severityFatal,
                            folderName,
                            workflowName,
                            "","","","",
                            "Workflow is scheduled to run continuously. Option run continuously is set inside the scheduler.",
                            "not checked",
                            "checked" )
            elif ((endoptions and endoptions.attrib.get('ENDTYPE') == "RUNFOREVER") and (endoptions.attrib.get('RUNFOREVER') == "YES")):
                logToReport(severityWarning,
                            folderName,
                            workflowName,
                            "","","","",
                            "Workflow will not be scheduled after the integration service restarts. See documentation for Informatica scheduler.",
                            "see documentation",
                            "N/A" )
            
            scheduleOptions = scheduleInfo.find('SCHEDULEOPTIONS')
            if (scheduleOptions):
                recurring = scheduleOptions.find('RECURRING')
                if ((recurring)
                    and recurring.attrib.get('DAYS') == "0"
                    and recurring.attrib.get('HOURS') == "0"
                    and recurring.attrib.get('MINUTES') < 10):
                        logToReport(severityWarning,
                            folderName,
                            workflowName,
                            "","","","",
                            "Workflow is scheduled to run too often (via Run every).",
                            "minutes: " + recurring.attrib.get('MINUTES'),
                            "reasonably (>10 minutes)")
                
                custom = scheduleOptions.find('CUSTOM')
                if (custom):
                    dailyFrequency = custom.find('DAILYFREQUENCY')
                    if (dailyFrequency and dailyFrequency.attrib.get('HOURS')):
                        if (dailyFrequency.attrib.get('HOURS') == "0" and dailyFrequency.attrib.get('MINUTES') < 10):
                            logToReport(severityFatal,
                                        folderName,
                                        workflowName,
                                        "","","","",
                                        "Workflow is scheduled to run too often (via Customized repeat).",
                                        "minutes: " + dailyFrequency.attrib.get('MINUTES'),
                                        "reasonably (>10 minutes)") 

# Processing for Mappings and Mapplets
def processMapping(folderName, mappingElement):
    """ Verifies all specifics of supplied Mapping. \n
    Takes: string folderName, XMLElement mappingElement
    """
    mappingName = mappingElement.attrib.get('NAME')
    transformations = mappingElement.findall('TRANSFORMATION')
    for trans in transformations:
        #print(6*"\t"+ trans.tag + ": " + trans.attrib.get('NAME'))
        processTransformation(folderName, mappingName, trans)
        
def processSession(folderName, workflowElement, sessionInstance):
    global severityFatal
    global severityError
    global severityInfo
    global severityWarning
    global xmlParserRoot
    global parameters
    workflowName = workflowElement.attrib.get('NAME')

    # Note: there can be multiple instances of the same session definition in workflow
    sessionName = sessionInstance.attrib.get('NAME') # name of the actual session instace as appears in workflow
    sessionTaskName = sessionInstance.attrib.get('TASKNAME') # name of definition of the session

    if (sessionName != sessionTaskName):
        print("DIFFERENCE IN NAMES: " + sessionName + " vs TASKNAME " + sessionTaskName + " - verify!")
    
    print(6*"\t" + "Session: " + sessionName)

    # to be able to process check on session I need to access the correct session definition
    session = sessionInstance

    if (sessionInstance.attrib.get('REUSABLE') == "YES"):
        # session is reusable and therefore exists directly beneth the FOLDER element
        session = xmlParserRoot.findall('SESSION[@NAME="' + sessionTaskName + '"]')
    else:
        # session is NOT reusable and therefore exists directly beneth the WORKFLOW element
        session = workflowElement.findall('SESSION[@NAME="' + sessionTaskName + '"]')
    # list of session's attribute elements
    attributes = session.findall('ATTRIBUTE')
    # list of value pairs in the hashmap
    sessionAttributes = {}

    # for each element create one entry in the hashmap
    for attribute in attributes:
        sessionAttributes.update({attribute.attrib.get('NAME') : attribute.attrib.get('VALUES')})
    
    # once the attributes from the session definition have been processed, overridden values from the sessin instance need to be applied
    for attribute in sessionInstance.findall('ATTRIBUTE'):
        sessionAttributes.update({attribute.attrib.get('NAME') : attribute.attrib.get('VALUES')})
    
    # more session parameters/attributes are stored in the session configuration, here pointer to it is gathered
	#<CONFIGREFERENCE REFOBJECTNAME ="default_session_config" TYPE ="Session config"/>
    configReference = session.findAll('CONFIGREFERENCE[@TYPE="Session config"]')

    config = xmlParserRoot.findall('CONFIG[@NAME="' + configReference.attrib.get('REFOBJECTNAME') + '"]')
    # config is allways stored on the same level (directly under the FOLDER element)
	# session definition stored in $session is located on the different place depending on reusable/notreusable status hence parent vs parent->parent
    if (sessionInstance.attrib.get('REUSABLE') == "YES"):
        config = workflowElement.findall('CONFIG[@NAME="' + configReference.attrib.get('REFOBJECTNAME') + '"]')

    # load configuration from the session config
    for attribute in config.findall('ATTRIBUTE'):
        sessionAttributes.update({attribute.attrib.get('NAME') : attribute.attrib.get('VALUES')})
    
    # load configuration which has been overwritten in the session's CONFIGREFERENCE element
    for attribute in configReference.findall('ATTRIBUTE'):
        sessionAttributes.update({attribute.attrib.get('NAME') : attribute.attrib.get('VALUES')})
    find = sessionName + r'(\._)*(\$PMWorkflowName|' + workflowName + r')\.log'
    if (re.match(str(find), sessionAttributes.get("Session Log File Name"), flags=re.I)):
        logToReport(severityFatal,
                    folderName,
                    workflowName,
                    sessionName,
                    "","","",
                    "Session log name and session name do not match.",
                    sessionName + ".log or " + sessionName +"_\$PMWorkflowName.log",
                    sessionAttributes.get("Session Log File Name"))

    find = r'&#x5c;'
    sessionAttributes.update({"Session Log File directory" : re.sub(str(find), r'\/', sessionAttributes.get("Session Log File directory"))})

    if (not ( re.match(str(r'\$PMSessionLogDir\/'+folderName), sessionAttributes.get("Session Log File directory"), flags=re.I)
            or re.match(r'\$PMSessionLogDir\/\$PMfolderName', sessionAttributes.get("Session Log File directory"), flags=re.I)
            or re.match(str(r'\/opt\/Informatica\/infa_shared\/SessLogs\/'+folderName), sessionAttributes.get("Session Log File directory"), flags=re.I)
            or re.match(r'\/opt\/Informatica\/infa_shared\/SessLogs\/\$PMFolderName', sessionAttributes.get("Session Log File directory"), flags=re.I))
    ):
        if (sessionAttributes.get("Session Log File directory") == "\$PMSessionLogDir/" or sessionAttributes.get("Session Log File directory") == "\$PMSessionLogDir"):
            if(parameters.get("\$PMSessionLogDir")):
                if (not (re.match(str(r'\/opt\/Informatica\/infa_shared\/SessLogs\/'+folderName), parameters.get("\$PMSessionLogDir"), flags=re.I) 
                        or re.match(str(r'\$PMRootDir\/SessLogs\/'+folderName), parameters.get("\$PMSessionLogDir"), flags=re.I))
                ):
                    logToReport(severityFatal,
                                folderName,
                                workflowName,
                                sessionName,
                                "","","",
                                "Session Log File Directory is improperly set in param file.",
                                "\$PMRootDir/SessLogs/"+ folderName+"/",
                                parameters.get("\$PMSessionLogDir"))
        
        pass

def processTasks(folderName, workflowElement):
    global severityFatal
    global severityError
    global severityInfo
    global severityWarning
    global xmlParserRoot
    workflowName = workflowElement.attrib.get('NAME')
    #tasks = []
    #tasks.append(workflowElement.findall('TASK'))
    tasks = workflowElement.findall('TASK')
    #print("TASKS ONLY")
    #print(tasks)
    # Original Perl script:
    #foreach my $taskInstance ($workflow->children('TASKINSTANCE[@REUSABLE="YES"]')) {
	#	#print "" .  $taskInstance->atts->{TASKNAME} . "\n";
	#	push(@tasks, $taskInstance->parent->parent->children('TASK[@NAME="' . $taskInstance->atts->{TASKNAME} . '"]'));
	#}
    #
    #

    # Go through the Reusable Tasks used in current workflow. If there are any, add their base tasks into the "tasks" list of elements
    for taskInstance in workflowElement.findall('.//TASKINSTANCE[@REUSABLE="YES"]'):  #./TASKINSTANCE/*[@REUSABLE="NO"]
        for taskEle in xmlParserRoot.findall('.//TASK[@NAME="' + taskInstance.attrib.get('TASKNAME') + '"]'):
            if not any(item.attrib.get('NAME') == taskEle.attrib.get('NAME') for item in tasks):
                tasks.append(taskEle)
            #else:
            #    print("DUPLICATE")
    
    
    for task in tasks:
        taskType = task.attrib.get('TYPE')
        taskName = task.attrib.get('NAME')
        if (taskType == "COMMAND"):
            commands = task.findall('VALUEPAIR')
            commandsCount = len(commands)
            
            if (commandsCount > 1):
                logToReport(severityInfo,
                            folderName,
                            workflowName,
                            "",
                            taskName,
                            "","",
                            "Multiple commands found in one command task. Use script or multiple command tasks instead.",
                            "1",
                            commandsCount)

                commandsCount = len(task.findall('.//ATTRIBUTE[@NAME="Fail taks if any command fails" and @VALUE="YES"]'))
                if (commandsCount != 1):
                    logToReport(severityError,
                                folderName,
                                workflowName,
                                "",
                                taskName,
                                "","",
                                "Fail task if any command fails not set.",
                                "YES",
                                "NO")
            elif (commandsCount == 0):
                logToReport(severityError,
                            folderName,
                            workflowName,
                            "",
                            taskName,
                            "","",
                            "Empty Command task found.",
                            "Non-empty",
                            "Empty")
                continue

            for command in commands:
                commandValue = command.attrib.get('VALUE')
                commandName = command.attrib.get('NAME')
                commandValue = replaceXMLChars(commandValue)

                if (re.match(r'(chmod\s+\d{0,3})', commandValue)):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "Chmod can not be used.", re.search(r'(chmod\s+\d{0,3})', commandValue).group(1), commandValue)
                if (re.match(r'((^|\s|;)rm\s+-?r?f?)', commandValue)):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "rm can be used only in the script.", re.search(r'((^|\s|;)rm\s+-?r?f?)', commandValue).group(1), commandValue)
                if (re.match(r'((^|\s|;)mv\s+-?f?)', commandValue)):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "mv can be used only in the script.", re.search(r'((^|\s|;)mv\s+-?f?)', commandValue).group(1), commandValue)
                if (re.match(r'(^\s*\/opt\/Informatica\/isource\/[\w\$\/]*)', commandValue, flags=re.I)):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "Nothing can not be run from isource.", "run from infa_shared", commandValue)
                
                # IMPORTANT/ TODO: this is check only for the way how the script is started. Check which interpreter is stated on the first line of script should be added
                if (re.match(r'^(k?sh\s)(.*)', commandValue, flags=re.I)):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "Non default shell is used. Do not specify the shell, use default or contact ETL administrators.", re.search(r'^(k?sh\s)(.*)', commandValue, flags=re.I).group(2), commandValue)
                if (re.match(r';|\|', commandValue)):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "Complex, chained command found, use script instead.", "use script", commandValue)
                find = r'> \$PMWorkflowLogDir/\$PMFolderName/' +taskName+ r'_`date +%Y%m%d%H%M%S`.log 2>&1'

                if (not re.match(str(find), commandValue, flags=re.I)):
                    logToReport(severityError, folderName, workflowName, "", taskName,"","", "Standard and error output of each command should be redirected into the log file.", find, commandValue)
                if (commandValue == ""):
                    logToReport(severityFatal, folderName, workflowName, "", taskName,"","", "Empty Command " + commandName + " found in task.", "non empty", "empty")
                else:
                    logToReport(severityInfo, folderName, workflowName, "", taskName,"","", "Information about command content: "+commandName, "only info", commandValue)

        elif (taskType == "DECISION"):
            if (task.findall('ATTRIBUTE')[0].attrib.get('VALUE') == ""):
                logToReport(severityError, folderName, workflowName, "", taskName,"","", "Empty Decission task found.", "non empty", "empty")
        elif (taskType == "ASSIGNMENT"):
            assignments = task.findall('VALUEPAIR')
            assignmentsCount = len(assignments)
            if (assignmentsCount == 0):
                logToReport(severityFatal, folderName, workflowName, "", taskName, "","", "Empty Assignment task found.", "non empty", "empty")
            else:
                for assignment in assignments:
                    if (assignment.attrib.get('VALUE') == ""):
                        logToReport(severityError, folderName, workflowName, "", taskName, "","", "Empty assignment " +assignment.attrib.get('NAME')+ " found in task.", "non empty", "empty")


def processTransformation(folderName, mappingName, transformation):
    """ Process Transformation, checking for EXPRESSION and JOINER type, validating if the type matches. \n
    Takes: string folderName, string mappingName, XMLElement transformation
    """
    global severityError
    global severityFatal
    global severityInfo
    transType = transformation.attrib.get('TYPE')

    if transType == 'EXPRESSION':
        transFields = transformation.findall('TRANFORMFIELD')
        for field in transFields:
            fieldPortType = field.attrib.get('PORTTYPE')
            if ( (fieldPortType == 'OUTPUT' or fieldPortType == 'LOCAL VARIABLE') and field.attrib.get('EXPRESSION') == '' ):
                logToReport(severityError,
                            folderName, 
                            "", 
                            "", 
                            "", 
                            mappingName, 
                            transformation.attrib.get('NAME'),  
                            "Empty OUTPUT port "+field.attrib.get('NAME')+" in expression", 
                            "non empty", 
                            "empty")
    elif transType == 'JOINER':
        tableAttributes = transformation.findall('TABLEATTRIBUTE')
        nameValue = {} # Hashmap of values
        for tableAttribute in tableAttributes:
            nameValue.update({tableAttribute.attrib.get('NAME'):tableAttribute.attrib.get('VALUE')})
        # Checking if Tracing is too verbose
        if (not (nameValue.get('Tracing Level') == 'NORMAL' or nameValue.get('Tracing Level') == 'TERSE')):
            logToReport(severityFatal,
                        folderName,
                        "",
                        "",
                        "",
                        mappingName,
                        transformation.attrib.get('NAME'),
                        "Tracing level too high.", 
                        "Normal / Terse", 
                        nameValue.get('Tracing Level'))

        if (nameValue.get('Sorted Input') == 'NO'):
            logToReport(severityInfo,
                        folderName,
                        "",
                        "",
                        "",
                        mappingName,
                        transformation.attrib.get('NAME'),
                        "Input for joiner is not pre-sorted.",
                        "Sorted",
                        "Not sorted")


def readParameterFile(paramFile):
    pass

def initializeXMLFile(stream_xmlFile, worksheetName):
    """ Create "Header style" elements in the supplied XML File styled as Excel File. \n
    Takes: string xmlFile, string worksheetName \n
    "xmlFile" should be full path to a file on the filesystem.
    """
    #global ENCODING
    #xmlFilePath = Path(xmlFile)
    conditionalRow = "<Column ss:AutoFitWidth=\"1\" ss:Width=\"150\"/>\n" if worksheetName != "Workflow" else ""
    #with open(xmlFilePath, 'a', encoding=ENCODING) as file:
    stream_xmlFile.write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<?mso-application progid=\"Excel.Sheet\"?>\n"+
                            "<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\" xmlns:html=\"http://www.w3.org/TR/REC-html40\">\n"+
                            "<Styles>\n"+
                            "<Style ss:ID=\"Default\" ss:Name=\"Normal\">\n"+
                            "<Alignment ss:Vertical=\"Bottom\" ss:WrapText=\"1\"/>\n"+
                            "</Style>\n"+
                            "</Styles>\n"+
                            "<Worksheet ss:Name=\"" + worksheetName + "\">\n"+
                            "<Table>\n"+
                            "<Column ss:AutoFitWidth=\"1\" ss:Width=\"30\"/>\n"+
                            "<Column ss:AutoFitWidth=\"1\" ss:Width=\"60\"/>\n"+
                            "<Column ss:AutoFitWidth=\"1\" ss:Width=\"100\"/>\n"+
                            "<Column ss:AutoFitWidth=\"1\" ss:Width=\"150\"/>\n"+
                            conditionalRow +
                            "<Column ss:AutoFitWidth=\"0\" ss:Width=\"400\"/>\n"+
                            "<Column ss:AutoFitWidth=\"0\" ss:Width=\"150\"/>\n"+
                            "<Column ss:AutoFitWidth=\"0\" ss:Width=\"300\"/>\n")
    

def finalizeXMLFile(xmlFile, rowCount):
    """ Finalize supplied XML File styled as Excel File, using "Footer style" elements. \n
    Takes: string xmlFile, int rowCount \n
    "xmlFile" should be full path to a file on the filesystem.
    """
    global ENCODING
    # xml excel where it needs to be written
    localXmlFile = Path(xmlFile)
    with open(localXmlFile, 'a', encoding=ENCODING) as file:
        file.write( "</Table>\n"+
                    "<AutoFilter x:Range=\"R1C1:R" + str(rowCount) + "C8\" xmlns=\"urn:schemas-microsoft-com:office:excel\"/>\n"+
                    "</Worksheet>\n"+
                    "</Workbook>\n")
    # rowCount     how many rows exist in the xml excel file, serves for proper filter range creation in xml excel

    
def printXMLCell(stream_xmlFile, cellContent):
    """ Write log entry into supplied XML file styled as Excel File in "CELL Element" format. \n
    Takes: string xmlFile, string cellContent \n
    "xmlFile" should be full path to a file on the filesystem.
    """
    global ENCODING
    cellContentLocal = ""#str(cellContent) if cellContent.isdigit else cellContent
    # Escape all possible XML non-allowed chars
    cellContentLocal = escapeXMLChars(cellContentLocal)
    stream_xmlFile.write( "<Cell><Data ss:Type=\"String\">" + cellContentLocal + "</Data></Cell>\n")


def logToReport(severity, folderName, workflowName, sessionName, taskName, mappingName, transformationName, issueDescription, expected, found):
    """Generic logging function for all object types based on input. \n
    For unused fields (opt), use "" as parameter (empty string). \n
    Takes: string severity, string folderName, string workflowName (opt), string sessionName (opt), string taskName (opt), string mappingName (opt),
    string transformationName (opt), string issueDescription, string expected, string found \n

    """
    global ENCODING
    # Log to WORKFLOW report
    if (sessionName == "" and taskName == "" and mappingName == "" and transformationName == ""):
        #workflow
        global wRows
        global stream_wfFileName
    
        localStream = stream_wfFileName # Generic definition for easier replacement
        # Header
        #with open(localFileName, 'a', encoding=ENCODING) as file:
        localStream.write("<Row>\n")
        printXMLCell(localStream, "ID" if wRows == 0 else str(wRows))

        # Generic prints
        printXMLCell(localStream, severity)
        printXMLCell(localStream, folderName)
        # Specific prints
        printXMLCell(localStream, workflowName)
        # Generic prints
        printXMLCell(localStream, issueDescription)
        printXMLCell(localStream, expected)
        printXMLCell(localStream, found)

        # Footer
        #with open(localFileName, 'a', encoding=ENCODING) as file:
        localStream.write("</Row>\n")
        wRows+=1
    # Log to SESSION Report
    elif(taskName == "" and mappingName == "" and transformationName == ""):
        #session
        global sRows
        global stream_sFileName
    
        localStream = stream_sFileName # Generic definition for easier replacement
        # Header
        localStream.write("<Row>\n")
        printXMLCell(localStream, "ID" if sRows == 0 else str(sRows))

        # Generic prints
        printXMLCell(localStream, severity)
        printXMLCell(localStream, folderName)
        # Specific prints
        printXMLCell(localStream, workflowName)
        printXMLCell(localStream, sessionName)
        # Generic prints
        printXMLCell(localStream, issueDescription)
        printXMLCell(localStream, expected)
        printXMLCell(localStream, found)

        # Footer
        localStream.write("</Row>\n")
        sRows+=1
    # Log to TASK Report
    elif(sessionName == "" and mappingName == "" and transformationName == ""):
        #Task
        global tRows
        global stream_tFileName
    
        localStream = stream_tFileName # Generic definition for easier replacement
        cellContent = ""
        if tRows == 0:
            cellContent = "ID"
        else:
            cellContent = tRows
        # Header
        localStream.write("<Row>\n")
        printXMLCell(localStream, cellContent) # TODO: Original Perl script has "else sRows" - is that correct???

        # Generic prints
        printXMLCell(localStream, severity)
        printXMLCell(localStream, folderName)
        # Specific prints
        printXMLCell(localStream, workflowName)
        printXMLCell(localStream, taskName)
        # Generic prints
        printXMLCell(localStream, issueDescription)
        printXMLCell(localStream, expected)
        printXMLCell(localStream, found)

        # Footer
        localStream.write("</Row>\n")
        tRows+=1
    #Log to MAPPING Report
    elif(workflowName == "" and sessionName == "" and taskName == ""):
        #Mapping
        global mRows
        global stream_mFileName
        localStream = stream_mFileName # Generic definition for easier replacement
        cellContent = "ID" if mRows == 0 else str(mRows)
        # Header
        localStream.write("<Row>\n")
        printXMLCell(localStream, cellContent) # TODO: Original Perl script has "else sRows" - is that correct???

        # Generic prints
        printXMLCell(localStream, severity)
        printXMLCell(localStream, folderName)
        # Specific prints
        printXMLCell(localStream, mappingName)
        printXMLCell(localStream, transformationName)
        # Generic prints
        printXMLCell(localStream, issueDescription)
        printXMLCell(localStream, expected)
        printXMLCell(localStream, found)

        # Footer
        localStream.write("</Row>\n")
        mRows+=1


def replaceXMLChars(inputString):
    """ Replaces XML escaped characters into their normal form. \n
    Takes: string inputString
    Returns: string
    """
    outputString = re.sub(r'&amp;',        r'&',   inputString)
    outputString = re.sub(r'&#xA;',        r'\n',   outputString)
    outputString = re.sub(r'&#xD;',        r'\n',   outputString)
    outputString = re.sub(r'&#x5c;',       r'\\',   outputString)  
    outputString = re.sub(r'&apos;',       r'\'',   outputString)
    outputString = re.sub(r'&quot;',       r'"',   outputString)
    outputString = re.sub(r'&lt;',         r'<',   outputString)
    outputString = re.sub(r'&gt;',         r'>',   outputString)
    return outputString

def escapeXMLChars(inputString):
    """ Escapes XML non-allowed characters. \n
    Takes: string inputString
    Returns: string
    """
    outputString = re.sub(r'&',    r'&amp;',         inputString)
    outputString = re.sub(r'\n',    r'&#xD;&#xA;',    outputString)
    outputString = re.sub(r'\\',    r'&#x5c;',        outputString)
    outputString = re.sub(r'\'',    r'&apos;',        outputString)
    outputString = re.sub(r'\"',    r'&quot;',        outputString)
    outputString = re.sub(r'<',    r'&lt;',          outputString)
    outputString = re.sub(r'>',    r'&gt;',          outputString)
    
    return outputString


'''
try:
    iter = 0 # Used to skip the root directory
    for dir in os.walk(inputDirectory):
        # Skip the root directory
        if iter == 0:
            iter+=1
            continue
        print("Processing "+dir[0])
        processInput(dir[0])
except Exception as e:
    print("Unexpected exception occured: "+str(e))
    
finally:
    stream_wfFileName.close()
    stream_sFileName.close()
    stream_tFileName.close()
    stream_mFileName.close()

'''

tempString = '&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xA;&lt;imx:IMX xmlns:imx=&quot;http://com.informatica.imx&quot; xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; serializationSpecVersion=&quot;6.0&quot; crcEnabled=&quot;0&quot; xmlns:adapter=&quot;http://com.informatica.cloud.api.adapter/1&quot; versionadapter=&quot;1.2.3&quot; xsi:schemaLocation=&quot;http://com.informatica.imx IMX.xsd http://com.informatica.cloud.api.adapter/1 com.informatica.cloud.api.adapter.xsd&quot;&gt;&#xA;&lt;adapter:LWReadOperation imx:id=&quot;U:AI6MJjvIEeiLd1SGEBwwPw&quot; connectionAttributes=&quot;U:AI6MJzvIEeiLd1SGEBwwPw U:AI6MKDvIEeiLd1SGEBwwPw U:AI6MKTvIEeiLd1SGEBwwPw U:AI6MKjvIEeiLd1SGEBwwPw U:AI6MKzvIEeiLd1SGEBwwPw U:AI6MLDvIEeiLd1SGEBwwPw&quot; desAttrList=&quot;U:AI6MLTvIEeiLd1SGEBwwPw&quot; pluginId=&quot;447700&quot; pluginShortName=&quot;AzureDW&quot; pluginVersion=&quot;U:AI6MLjvIEeiLd1SGEBwwPw&quot; primaryRecordInfo=&quot;U:AI6MLzvIEeiLd1SGEBwwPw&quot; runAttrList=&quot;U:AI6MMDvIEeiLd1SGEBwwPw U:AI6MMTvIEeiLd1SGEBwwPw U:AI6MMjvIEeiLd1SGEBwwPw&quot;&gt;&#xA;&lt;sqlSpec imx:id=&quot;ID_1&quot; xsi:type=&quot;adapter:SQLSpecification&quot;/&gt;&#xA;&lt;/adapter:LWReadOperation&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:AI6MJzvIEeiLd1SGEBwwPw&quot; attrId=&quot;4&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+JDBC+URL&quot; type=&quot;7&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:AI6MKDvIEeiLd1SGEBwwPw&quot; attrId=&quot;5&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+JDBC+Username&quot; type=&quot;7&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:AI6MKTvIEeiLd1SGEBwwPw&quot; attrId=&quot;6&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+JDBC+Password&quot; type=&quot;32&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:AI6MKjvIEeiLd1SGEBwwPw&quot; attrId=&quot;3&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+Schema+Name&quot; type=&quot;7&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:AI6MKzvIEeiLd1SGEBwwPw&quot; attrId=&quot;1&quot; isMandatory=&quot;true&quot; name=&quot;Azure+Blob+Account+Name&quot; type=&quot;7&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:AI6MLDvIEeiLd1SGEBwwPw&quot; attrId=&quot;2&quot; isMandatory=&quot;true&quot; name=&quot;Azure+Blob+Account+Key&quot; type=&quot;32&quot;/&gt;&#xA;&lt;adapter:DesigntimeAttribute imx:id=&quot;U:AI6MLTvIEeiLd1SGEBwwPw&quot; attrId=&quot;61&quot; datatype=&quot;STRING&quot; defaultValue=&quot;PWX_AZUREDW&quot; description=&quot;PC_LICENCE_KEY&quot; name=&quot;PC_LICENCE_KEY&quot;/&gt;&#xA;&lt;adapter:MetadataVersion imx:id=&quot;U:AI6MLjvIEeiLd1SGEBwwPw&quot; build=&quot;1&quot; major=&quot;1&quot;/&gt;&#xA;&lt;adapter:RecordInfo imx:id=&quot;U:AI6MLzvIEeiLd1SGEBwwPw&quot; catalogName=&quot;CC_IM_POLKLU&quot; label=&quot;VW_IN_DELETE_CHANGED_DWH&quot; name=&quot;VW_IN_DELETE_CHANGED_DWH&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:AI6MMDvIEeiLd1SGEBwwPw&quot; attrId=&quot;7&quot; datatype=&quot;STRING&quot; description=&quot;Name+of+Azure+Blob+Container&quot; isMandatory=&quot;true&quot; name=&quot;Azure+Blob+Container+Name&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:AI6MMTvIEeiLd1SGEBwwPw&quot; attrId=&quot;8&quot; datatype=&quot;STRING&quot; defaultValue=&quot;%2C&quot; description=&quot;Delimiter+to+be+used+in+the+CSV+File&quot; isMandatory=&quot;true&quot; name=&quot;Field+Delimiter&quot; userValue=&quot;%2C&quot; listOfValues=&quot;%2C %7C %60 %7E TAB&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:AI6MMjvIEeiLd1SGEBwwPw&quot; attrId=&quot;9&quot; datatype=&quot;STRING&quot; defaultValue=&quot;4&quot; description=&quot;The+number+of+concurrent+connections+to+the+Blob+Store+to+upload%2Fdownload+files&quot; name=&quot;Number+of+concurrent+connections+to+Blob+Store&quot; userValue=&quot;4&quot; listOfValues=&quot;1 2 4 8 10&quot;/&gt;&#xA;&lt;/imx:IMX&gt;&#xA;'
targetString = '&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#xA;&lt;imx:IMX xmlns:imx=&quot;http://com.informatica.imx&quot; xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot; serializationSpecVersion=&quot;10.0&quot; crcEnabled=&quot;0&quot; xmlns:adapter=&quot;http://com.informatica.cloud.api.adapter/1&quot; versionadapter=&quot;1.2.4&quot; xsi:schemaLocation=&quot;http://com.informatica.imx IMX.xsd http://com.informatica.cloud.api.adapter/1 com.informatica.cloud.api.adapter.xsd&quot;&gt;&#xA;&lt;adapter:LWReadOperation imx:id=&quot;U:IvGvJnLtEeiCa_vvqcr37A&quot; connectionAttributes=&quot;U:IvGvJ3LtEeiCa_vvqcr37A U:IvGvKHLtEeiCa_vvqcr37A U:IvGvKXLtEeiCa_vvqcr37A U:IvGvKnLtEeiCa_vvqcr37A U:IvGvK3LtEeiCa_vvqcr37A U:IvGvLHLtEeiCa_vvqcr37A&quot; desAttrList=&quot;U:IvGvLXLtEeiCa_vvqcr37A&quot; pluginId=&quot;449400&quot; pluginShortName=&quot;AzureDWv2&quot; pluginVersion=&quot;U:IvGvLnLtEeiCa_vvqcr37A&quot; primaryRecordInfo=&quot;U:IvGvL3LtEeiCa_vvqcr37A&quot; runAttrList=&quot;U:IvGvMHLtEeiCa_vvqcr37A U:IvGvMXLtEeiCa_vvqcr37A U:IvGvMnLtEeiCa_vvqcr37A U:IvGvM3LtEeiCa_vvqcr37A U:IvGvNHLtEeiCa_vvqcr37A U:IvGvNXLtEeiCa_vvqcr37A U:IvGvNnLtEeiCa_vvqcr37A U:IvGvN3LtEeiCa_vvqcr37A&quot;&gt;&#xA;&lt;sqlSpec imx:id=&quot;ID_1&quot; xsi:type=&quot;adapter:SQLSpecification&quot;/&gt;&#xA;&lt;/adapter:LWReadOperation&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:IvGvJ3LtEeiCa_vvqcr37A&quot; attrId=&quot;1&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+JDBC+URL&quot; type=&quot;2&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:IvGvKHLtEeiCa_vvqcr37A&quot; attrId=&quot;2&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+JDBC+Username&quot; type=&quot;2&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:IvGvKXLtEeiCa_vvqcr37A&quot; attrId=&quot;3&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+JDBC+Password&quot; type=&quot;32&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:IvGvKnLtEeiCa_vvqcr37A&quot; attrId=&quot;4&quot; isMandatory=&quot;true&quot; name=&quot;Azure+DW+Schema+Name&quot; type=&quot;2&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:IvGvK3LtEeiCa_vvqcr37A&quot; attrId=&quot;5&quot; isMandatory=&quot;true&quot; name=&quot;Azure+Blob+Account+Name&quot; type=&quot;2&quot;/&gt;&#xA;&lt;adapter:ConnectionAttribute imx:id=&quot;U:IvGvLHLtEeiCa_vvqcr37A&quot; attrId=&quot;6&quot; isMandatory=&quot;true&quot; name=&quot;Azure+Blob+Account+Key&quot; type=&quot;32&quot;/&gt;&#xA;&lt;adapter:DesigntimeAttribute imx:id=&quot;U:IvGvLXLtEeiCa_vvqcr37A&quot; attrId=&quot;61&quot; datatype=&quot;STRING&quot; defaultValue=&quot;PWX_AZUREDW&quot; description=&quot;PC_LICENCE_KEY&quot; name=&quot;PC_LICENCE_KEY&quot;/&gt;&#xA;&lt;adapter:MetadataVersion imx:id=&quot;U:IvGvLnLtEeiCa_vvqcr37A&quot; major=&quot;1&quot;/&gt;&#xA;&lt;adapter:RecordInfo imx:id=&quot;U:IvGvL3LtEeiCa_vvqcr37A&quot; catalogName=&quot;CC_DM_POLKLU&quot; label=&quot;POLIS_KLUIS_ANALYSE&quot; name=&quot;POLIS_KLUIS_ANALYSE&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvMHLtEeiCa_vvqcr37A&quot; attrId=&quot;1&quot; datatype=&quot;STRING&quot; description=&quot;Name+of+Azure+Blob+Container+to+be+used+for+staging&quot; isMandatory=&quot;true&quot; maxLength=&quot;255&quot; name=&quot;Azure+Blob+Container+Name&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvMXLtEeiCa_vvqcr37A&quot; attrId=&quot;2&quot; datatype=&quot;STRING&quot; defaultValue=&quot;0x1e&quot; description=&quot;Delimiter+to+be+used+for+separating+fields+or+columns&quot; isMandatory=&quot;true&quot; maxLength=&quot;255&quot; name=&quot;Field+Delimiter&quot; userValue=&quot;0x1e&quot; listOfValues=&quot;0x1e %2C %7C %60 %7E TAB&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvMnLtEeiCa_vvqcr37A&quot; attrId=&quot;3&quot; datatype=&quot;STRING&quot; defaultValue=&quot;4&quot; description=&quot;Number+of+concurrent+connections+to+Blob+Store+for+downloading+the+staging+file.&quot; isMandatory=&quot;true&quot; maxLength=&quot;255&quot; name=&quot;Number+of+concurrent+connections+to+Blob+Store&quot; userValue=&quot;4&quot; listOfValues=&quot;1 2 4 8 10&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvM3LtEeiCa_vvqcr37A&quot; attrId=&quot;4&quot; datatype=&quot;STRING&quot; description=&quot;Adapter+will+execute+this+sql+before+mapping+session+starts.&quot; maxLength=&quot;5999&quot; name=&quot;Pre+SQL&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvNHLtEeiCa_vvqcr37A&quot; attrId=&quot;5&quot; datatype=&quot;STRING&quot; description=&quot;Adapter+will+execute+this+sql+after+mapping+session+is+completed.&quot; maxLength=&quot;5999&quot; name=&quot;Post+SQL&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvNXLtEeiCa_vvqcr37A&quot; attrId=&quot;6&quot; datatype=&quot;STRING&quot; description=&quot;SQL+Query+Override.+Adapter+will+use+only+the+entire+query+to+fetch+data+from+Database.&quot; maxLength=&quot;5999&quot; name=&quot;SQL+Override&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvNnLtEeiCa_vvqcr37A&quot; attrId=&quot;7&quot; datatype=&quot;STRING&quot; defaultValue=&quot;0x1f&quot; description=&quot;The+quote+character%2Fstring+delimiter+to+be+used+for+data+movement.&quot; isMandatory=&quot;true&quot; maxLength=&quot;49&quot; name=&quot;Quote+Character&quot; userValue=&quot;0x1f&quot;/&gt;&#xA;&lt;adapter:RuntimeAttribute imx:id=&quot;U:IvGvN3LtEeiCa_vvqcr37A&quot; attrId=&quot;8&quot; datatype=&quot;STRING&quot; description=&quot;Partition+override-able+filter+query+to+divide+the+source+data+into+segments.&quot; maxLength=&quot;2599&quot; name=&quot;INFA+Advanced+Filter&quot;/&gt;&#xA;&lt;/imx:IMX&gt;&#xA;'

targetString2 = '&amp;lt;?xml version=&amp;quot;1.0&amp;quot; encoding=&amp;quot;UTF-8&amp;quot;?&amp;gt;&amp;#xA;&amp;lt;imx:IMX xmlns:imx=&amp;quot;http://com.informatica.imx&amp;quot; xmlns:xsi=&amp;quot;http://www.w3.org/2001/XMLSchema-instance&amp;quot; serializationSpecVersion=&amp;quot;10.0&amp;quot; crcEnabled=&amp;quot;0&amp;quot; xmlns:adapter=&amp;quot;http://com.informatica.cloud.api.adapter/1&amp;quot; versionadapter=&amp;quot;1.2.4&amp;quot; xsi:schemaLocation=&amp;quot;http://com.informatica.imx IMX.xsd http://com.informatica.cloud.api.adapter/1 com.informatica.cloud.api.adapter.xsd&amp;quot;&amp;gt;&amp;#xA;&amp;lt;adapter:LWReadOperation imx:id=&amp;quot;U:XnL9ZnLtEeizHSW45xxsvg&amp;quot; connectionAttributes=&amp;quot;U:XnL9Z3LtEeizHSW45xxsvg U:XnL9aHLtEeizHSW45xxsvg U:XnL9aXLtEeizHSW45xxsvg U:XnL9anLtEeizHSW45xxsvg U:XnL9a3LtEeizHSW45xxsvg U:XnL9bHLtEeizHSW45xxsvg&amp;quot; desAttrList=&amp;quot;U:XnL9bXLtEeizHSW45xxsvg&amp;quot; pluginId=&amp;quot;449400&amp;quot; pluginShortName=&amp;quot;AzureDWv2&amp;quot; pluginVersion=&amp;quot;U:XnL9bnLtEeizHSW45xxsvg&amp;quot; primaryRecordInfo=&amp;quot;U:XnL9b3LtEeizHSW45xxsvg&amp;quot; runAttrList=&amp;quot;U:XnL9cHLtEeizHSW45xxsvg U:XnL9cXLtEeizHSW45xxsvg U:XnL9cnLtEeizHSW45xxsvg U:XnL9c3LtEeizHSW45xxsvg U:XnL9dHLtEeizHSW45xxsvg U:XnL9dXLtEeizHSW45xxsvg U:XnL9dnLtEeizHSW45xxsvg U:XnL9d3LtEeizHSW45xxsvg&amp;quot;&amp;gt;&amp;#xA;&amp;lt;sqlSpec imx:id=&amp;quot;ID_1&amp;quot; xsi:type=&amp;quot;adapter:SQLSpecification&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;/adapter:LWReadOperation&amp;gt;&amp;#xA;&amp;lt;adapter:ConnectionAttribute imx:id=&amp;quot;U:XnL9Z3LtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;1&amp;quot; isMandatory=&amp;quot;true&amp;quot; name=&amp;quot;Azure+DW+JDBC+URL&amp;quot; type=&amp;quot;2&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:ConnectionAttribute imx:id=&amp;quot;U:XnL9aHLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;2&amp;quot; isMandatory=&amp;quot;true&amp;quot; name=&amp;quot;Azure+DW+JDBC+Username&amp;quot; type=&amp;quot;2&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:ConnectionAttribute imx:id=&amp;quot;U:XnL9aXLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;3&amp;quot; isMandatory=&amp;quot;true&amp;quot; name=&amp;quot;Azure+DW+JDBC+Password&amp;quot; type=&amp;quot;32&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:ConnectionAttribute imx:id=&amp;quot;U:XnL9anLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;4&amp;quot; isMandatory=&amp;quot;true&amp;quot; name=&amp;quot;Azure+DW+Schema+Name&amp;quot; type=&amp;quot;2&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:ConnectionAttribute imx:id=&amp;quot;U:XnL9a3LtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;5&amp;quot; isMandatory=&amp;quot;true&amp;quot; name=&amp;quot;Azure+Blob+Account+Name&amp;quot; type=&amp;quot;2&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:ConnectionAttribute imx:id=&amp;quot;U:XnL9bHLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;6&amp;quot; isMandatory=&amp;quot;true&amp;quot; name=&amp;quot;Azure+Blob+Account+Key&amp;quot; type=&amp;quot;32&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:DesigntimeAttribute imx:id=&amp;quot;U:XnL9bXLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;61&amp;quot; datatype=&amp;quot;STRING&amp;quot; defaultValue=&amp;quot;PWX_AZUREDW&amp;quot; description=&amp;quot;PC_LICENCE_KEY&amp;quot; name=&amp;quot;PC_LICENCE_KEY&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:MetadataVersion imx:id=&amp;quot;U:XnL9bnLtEeizHSW45xxsvg&amp;quot; major=&amp;quot;1&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RecordInfo imx:id=&amp;quot;U:XnL9b3LtEeizHSW45xxsvg&amp;quot; catalogName=&amp;quot;CC_IM_POLKLU&amp;quot; label=&amp;quot;VW_IN_DELETE_CHANGED_DWH&amp;quot; name=&amp;quot;VW_IN_DELETE_CHANGED_DWH&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9cHLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;1&amp;quot; datatype=&amp;quot;STRING&amp;quot; description=&amp;quot;Name+of+Azure+Blob+Container+to+be+used+for+staging&amp;quot; isMandatory=&amp;quot;true&amp;quot; maxLength=&amp;quot;255&amp;quot; name=&amp;quot;Azure+Blob+Container+Name&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9cXLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;2&amp;quot; datatype=&amp;quot;STRING&amp;quot; defaultValue=&amp;quot;0x1e&amp;quot; description=&amp;quot;Delimiter+to+be+used+for+separating+fields+or+columns&amp;quot; isMandatory=&amp;quot;true&amp;quot; maxLength=&amp;quot;255&amp;quot; name=&amp;quot;Field+Delimiter&amp;quot; userValue=&amp;quot;0x1e&amp;quot; listOfValues=&amp;quot;0x1e %2C %7C %60 %7E TAB&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9cnLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;3&amp;quot; datatype=&amp;quot;STRING&amp;quot; defaultValue=&amp;quot;4&amp;quot; description=&amp;quot;Number+of+concurrent+connections+to+Blob+Store+for+downloading+the+staging+file.&amp;quot; isMandatory=&amp;quot;true&amp;quot; maxLength=&amp;quot;255&amp;quot; name=&amp;quot;Number+of+concurrent+connections+to+Blob+Store&amp;quot; userValue=&amp;quot;4&amp;quot; listOfValues=&amp;quot;1 2 4 8 10&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9c3LtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;4&amp;quot; datatype=&amp;quot;STRING&amp;quot; description=&amp;quot;Adapter+will+execute+this+sql+before+mapping+session+starts.&amp;quot; maxLength=&amp;quot;5999&amp;quot; name=&amp;quot;Pre+SQL&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9dHLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;5&amp;quot; datatype=&amp;quot;STRING&amp;quot; description=&amp;quot;Adapter+will+execute+this+sql+after+mapping+session+is+completed.&amp;quot; maxLength=&amp;quot;5999&amp;quot; name=&amp;quot;Post+SQL&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9dXLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;6&amp;quot; datatype=&amp;quot;STRING&amp;quot; description=&amp;quot;SQL+Query+Override.+Adapter+will+use+only+the+entire+query+to+fetch+data+from+Database.&amp;quot; maxLength=&amp;quot;5999&amp;quot; name=&amp;quot;SQL+Override&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9dnLtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;7&amp;quot; datatype=&amp;quot;STRING&amp;quot; defaultValue=&amp;quot;0x1f&amp;quot; description=&amp;quot;The+quote+character%2Fstring+delimiter+to+be+used+for+data+movement.&amp;quot; isMandatory=&amp;quot;true&amp;quot; maxLength=&amp;quot;49&amp;quot; name=&amp;quot;Quote+Character&amp;quot; userValue=&amp;quot;0x1f&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;adapter:RuntimeAttribute imx:id=&amp;quot;U:XnL9d3LtEeizHSW45xxsvg&amp;quot; attrId=&amp;quot;8&amp;quot; datatype=&amp;quot;STRING&amp;quot; description=&amp;quot;Partition+override-able+filter+query+to+divide+the+source+data+into+segments.&amp;quot; maxLength=&amp;quot;2599&amp;quot; name=&amp;quot;INFA+Advanced+Filter&amp;quot;/&amp;gt;&amp;#xA;&amp;lt;/imx:IMX&amp;gt;&amp;#xA;'
#print (replaceXMLChars(tempString))
print (5*"\n")
print (replaceXMLChars(targetString))
print (5*"\n")
#rint (replaceXMLChars(targetString2))