#!/bin/bash
#   This script takes input file and processes each line, creating/updating/deleting specified OS Profiles based on input data.
#   Created by: Daniel Havlicek (Infa Partner)
#   First version: 02.06.2021
#   Changelog:  02.09.2021 - (DH) - Added static parameters into "diso" parameter for IDQ folder paths
#               03.09.2021 - (DH) - Added logic to handle "branch" from a base "trunk" project
#               20.09.2021 - (DH) - Added handling logic for ETL CL user to not reset password when creating OSP for branches
#               04.10.2021 - (DH) - ETL_CL user now gets permission for the OSP object
#               19.11.2021 - (DH) - Added new run mode type CHECK - does not create any OSP/users, but just checks if data integrity is fine
#
#
#   Overview of all actions:
#   1. 
#   2. 
#   3. 
#   4. 
#   5. 
#   6. 
#   7. 
#   8. 
#   9. 

# RUN_MODE;PROJECT_NAME;OSP_NAME;SYSTEM_USERNAME;OSP_GROUP;DQ_OSP_GROUP;ENVIRONMENT_VARIABLES;DQ_ENVIRONMENT_VARIABLES;REPOSITORY_PERMISSIONS
# CREATE;MICHA_00_LTEST_00;OSP_MICHALTEST;npadzyapp0080;DI_MICHA_00_LTEST_00;DQ_MICHA_00_LTEST_00;;;rwx

if [[ "$1" == "" || "$2" == "" || "$3" == "" || ! "$3" =~ "/" ]]; then
    echo "ERROR: One or more input parameters is empty!"
    echo "HOWTO: run this script with following parameters:"
    echo "1: DOMAIN_NAME - specify domain name to be used for creating OS Profiles"
    echo "2: MODEL_REP_SVC - specify Model Repository Service name"
    echo "3: INPUT_FILE - csv file with all data using following header: RUN_MODE;PROJECT_NAME;OSP_NAME;SYSTEM_USERNAME;OSP_GROUP;DQ_OSP_GROUP;ENVIRONMENT_VARIABLES;DQ_ENVIRONMENT_VARIABLES;REPOSITORY_PERMISSIONS"
    echo "INPUT_FILE should be specified as full path to given file."
    echo "Valid values for RUN_MODE: CREATE/DELETE/REMOVE/UPDATE/CHECK"
    echo "Valid values for PROJECT_NAME: MICHA_00_LTEST_00"
    echo "Valid values for OSP_NAME: OSP_MICHALTEST"
    echo "Valid values (has to be lowercase) for SYSTEM_USERNAME: npadzyapp0080"
    echo "Valid values for OSP_GROUP: DI_MICHA_00_LTEST_00"
    echo "Valid values for DQ_OSP_GROUP: DQ_MICHA_00_LTEST_00"
    echo "Default ENVIRONMENT_VARIABLES which are NOT needed to specify: '\$PMRootDir' 'USERCL<PROJECTNAME>NAME' 'USERCL<PROJECTNAME>PASS'"
    echo "Valid values for ENVIRONMENT_VARIABLES: '\$PMRootDir'='/app/ipc_data','\$PMWorkflowLogDir'='/app/ipc_data/WorkflowLogs',..."
    echo "Valid values for DQ_ENVIRONMENT_VARIABLES: '\$DISRootDir'='/app/idq_data','\$DISLogDir'='/app/idq_data/disLogs',..."
    echo "Valid values for REPOSITORY_PERMISSIONS: rwx / r-x / r--"
    echo "Terminating..."
    exit 1
fi

function log () {
    echo "[`date '+%F %H:%M:%S'`] $1"
}

function logDev () {
    if [[ "$isDev" == "true" ]]; then
        log "$1"
    fi
}

function htmlparse () {
    local IFS='>'
    read -d '<' TAG VALUE
}


# This function will run infacmd.sh command with specified parameters
# Parameters: $1=indexOfRow, $2=commandParamaters
function f_infacmdCustom () {
    if [[ "$2" != "" ]]; then
        infaCmdFull="infacmd.sh $2" # AssignGroupPermission -dn $2 -on $5 -ot $4 -eg $3

        logDev "Running infacmd command:"
        logDev "Command: $infaCmdFull"

        cmdResultCode=0
        cmdResult=$(eval $infaCmdFull; cmdResultCode=$?;)
        
        logDev "Command Result: $cmdResult"
        logDev "Command Result Code: $cmdResultCode"

        if [[ "$cmdResultCode" == "0" && ! "$cmdResult" =~ "Type help:" && ! "$cmdResult" =~ "failed with error" ]]; then
            logDev "Command infacmd finished successfully! Row: $1, Parameters: $2, ResultCode: $cmdResultCode" #Row: $1, ObjectType: $4, Object: $5, Group: $3
            return 0
        else
            log "ERROR: Command infacmd failed! Row: $1, Parameters: $2, ResultCode: $cmdResultCode"
            log "Result: $cmdResult"
            return 1
        fi
    fi
    return 2
}

# assignRepositoryGroupPermission($RowIndex $DomainName $AssignmentGroup $ObjectType $ObjectName $RepositoryPermissions "$isDev" == "true")
# This function will assign group permission to given $ObjectName based on its $ObjectType. Group for assignment is taken from $AssignmentGroup parameter
function f_pmrepCustom () {
    if [[ "$3" != "" ]]; then
        pmrepOSP="pmrep $2 $3"

        logDev "Running pmrep command:"
        logDev "Command: $pmrepOSP"

        cmdResultCode=0
        cmdResult=$(eval $pmrepOSP; cmdResultCode=$?;)
        
        logDev "Command Result: $cmdResult"
        logDev "Command Result Code: $cmdResultCode"

        if [[ "$cmdResultCode" == "0" && ! "$cmdResult" =~ "Usage:" ]]; then
            logDev "Repository Group Assigned successfully! Row: $1, Command: $2, Parameters: $3, ResultCode: $cmdResultCode"
            return 0
        else
            log "ERROR: Command f_pmrepCustom failed! Row: $1, Command: $2, Parameters: $3, ResultCode: $cmdResultCode"
            log "Result: $cmdResult"
            return 1
        fi
    fi
    return 2
}

DOMAIN_NAME=$1
MODEL_REP_SVC=$2
INPUT_FILE=$3
ProjectDirNameMask="<ProjectDirName>"
NewRootDir="/app/ipc_data"
NewDQRootDir="/app/idq_data"
ProcessedLinesFile="$INPUT_FILE.processed"
SkippedLinesFile="$INPUT_FILE.skipped"
LOG_FILE="$INPUT_FILE_`date +%F`.log"
EMPTY_STRING="NULL"

echo "Logging all output to file $LOG_FILE"
exec &>> $LOG_FILE
isDev=$( if [[ "$DOMAIN_NAME" == "devdom_eng" ]]; then echo "true"; else echo ""; fi )

# Create arrays from parsing the input file
arrRunType=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f1)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f1)"; fi; done ) )
arrProjectName=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f2)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f2)"; fi; done ) )
arrOSPName=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f3)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f3)"; fi; done ) )
arrSysUsername=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f4)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f4)"; fi; done ) )
arrOSPGroup=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f5)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f5)"; fi; done ) )
arrDQOSPGroup=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f6)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f6)"; fi; done ) )
arrEnvVariables=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f7)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f7)"; fi; done ) )
arrDQEnvVariables=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f8)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f8)"; fi; done ) )
arrGroupRepositoryPermissions=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ';' -f9)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ';' -f9)"; fi; done ) )
 

log "Process started..."

log "Creating log files for processed/skipped files:"
logDev "$LOG_FILE"
logDev "$ProcessedLinesFile"
logDev "$SkippedLinesFile"
logDev "arrGroupRepositoryPermissions:$arrGroupRepositoryPermissions"

echo "$(cat $INPUT_FILE | head -1)" > $ProcessedLinesFile
echo "$(cat $INPUT_FILE | head -1)" > $SkippedLinesFile

if [[ ! -f "$ProcessedLinesFile" || ! -f "$SkippedLinesFile" || ! -f "$LOG_FILE" ]]; then
    log "ERROR: Cannot create log files!"
    log "Terminating..."
    exit 1
fi

# Save list of all current groups
listOfAllGroups=$( infacmd.sh listAllGroups -dn $DOMAIN_NAME | head -n -1 )
listOfAllOSP=$( infacmd.sh listOSProfiles -dn $DOMAIN_NAME | head -n -1 )
listOfAllUsers=$( infacmd.sh ListAllUsers -dn $DOMAIN_NAME | head -n -1 )
listOfAllProjects=$( pmrep ListObjects -o Folder )
listOfAllConnections=$( pmrep listconnections | tail -n +9 | head -n -3 )


# Do for all array elements
for (( index = 1; index < ${#arrProjectName[@]}; index++ ))
do 
    runType="${arrRunType[$index]}"
    projectName="${arrProjectName[$index]}"
    OSPName="${arrOSPName[$index]}"
    sysUsername="${arrSysUsername[$index]}"
    OSPGroup="${arrOSPGroup[$index]}"
    DQOSPGroup="${arrDQOSPGroup[$index]}"
    envVariables="${arrEnvVariables[$index]}"
    DQEnvVariables="${arrDQEnvVariables[$index]}"
    groupRepositoryPermissions="${arrGroupRepositoryPermissions[$index]}"
    commandOSPRunType=""
    # Check if current project is a branch - it will have leading number not equal to 00, project is considered trunk if it contains no numbers at all as well
    branchNumber="$(echo "$projectName" | cut -d'_' -f2 | cut -d'_' -f1)"
    projectNumber="$(echo "$projectName" | cut -d'_' -f4 | cut -d'_' -f1)"
    isBranch=$( echo "$( if [[ "$branchNumber" == +([0-9]) && "$branchNumber" != "00" ]]; then echo "1"; else echo "0"; fi )" )

    log "Processing row $index, RunType: $runType, project: $projectName, OSP: $OSPName"

    fullLine="$runType;$projectName;$OSPName;$sysUsername;$OSPGroup;$DQOSPGroup;$envVariables;$DQEnvVariables;$groupRepositoryPermissions"
    logDev "$fullLine"
    
    log "Checking data integrity..."
    if [[ "$runType" == "$EMPTY_STRING" ]]; then log "ERROR: RUN_TYPE is empty! Skipping row $index"; continue; fi
    if [[ "$projectName" == "$EMPTY_STRING" ]]; then log "ERROR: Project Name is empty! Skipping row $index"; continue; fi
    if [[ "$OSPName" == "$EMPTY_STRING" ]]; then log "ERROR: OS Profile Name is empty! Skipping row $index"; continue; fi
    if  [[ "$runType" != "DELETE" && "$runType" != "REMOVE" ]]; then
        # Check for sysUsername only if not deleting the OSP
        if [[ "$sysUsername" == "$EMPTY_STRING" && "$runType" != "UPDATE" ]]; then log "ERROR: System username is empty! Skipping row $index"; continue; fi
        # Check for OSP Groups only if not deleting the OSP
        if [[ "$OSPGroup" == "$EMPTY_STRING" && "$DQOSPGroup" == "$EMPTY_STRING" ]]; then log "ERROR: No OSP Group is specified, skipping row $index"; continue; fi
    fi
    
    disoString="'\$DISRootDir'='$NewDQRootDir' '\$DISTempDir'='\$DISRootDir/disTemp/$projectName' '\$DISCacheDir'='\$DISRootDir/cache/$projectName' '\$DISSourceDir'='\$DISRootDir/source/$projectName' '\$DISTargetDir'='\$DISRootDir/target/$projectName' '\$DISRejectedFilesDir'='\$DISRootDir/reject/$projectName' '\$DISLogDir'='\$DISRootDir/disLogs/$projectName' "
    case "$runType" in
        "CREATE")
            commandOSPRunType="createOSProfile -po '\$PMRootDir'='$NewRootDir' -diso $disoString"
            ;;
        "DELETE")
            commandOSPRunType="removeOSProfile"
            ;;
        "REMOVE")
            commandOSPRunType="removeOSProfile"
            ;;
        "UPDATE")
            commandOSPRunType="updateOSProfile -po '\$PMRootDir'='$NewRootDir' -diso $disoString"
            ;;
        "CHECK")
            #Nothing needs to be done here
            ;;
        *)
            log "ERROR: Wrong RUN_TYPE specified! - $runType"
            log "Allowed values are:"
            log "CREATE"
            log "DELETE or REMOVE"
            log "UPDATE"
            log "CHECK"
            log "Skipping row $index"
            echo "$fullLine" >> $SkippedLinesFile
            continue
    esac
    log "Data integrity OK!"

    log "Starting Repository checks..."
    if [[ "$listOfAllProjects" =~ "$projectName" ]]; then
        logDev "Project $projectName exists in repository..."
    else
        log "ERROR: Project $projectName does NOT exist in repository! Skipping row $index..."
        echo "$fullLine" >> $SkippedLinesFile
        continue
    fi
    log "Repository checks finished!"

    if [[ "$runType" != "DELETE" && "$runType" != "REMOVE" ]]; then
        log "Starting Group checks..."
        if [[ "$OSPGroup" != "$EMPTY_STRING" ]]; then
            if [[ "$listOfAllGroups" =~ "$OSPGroup" ]]; then
                logDev "Group $OSPGroup exists in repository..."
            else
                # Might remove if we are gonna create the group
                log "ERROR: Group $OSPGroup does NOT exist in repository! Skipping row $index..."
                echo "$fullLine" >> $SkippedLinesFile
                continue
                # Create group
                # log "WARNING: Group $OSPGroup does NOT exist in repository, going to create it now..."
                # TBD
            fi
        fi

        if [[ "$DQOSPGroup" != "$EMPTY_STRING" ]]; then
            if [[ "$listOfAllGroups" =~ "$DQOSPGroup" ]]; then
                logDev "Group $DQOSPGroup exists in repository..."
            else
                # Might remove if we are gonna create the group
                log "ERROR: Group $DQOSPGroup does NOT exist in repository! Skipping row $index..."
                echo "$fullLine" >> $SkippedLinesFile
                continue
                # Create group
                #log "WARNING: Group $DQOSPGroup does NOT exist in repository, going to create it now..."
                # TBD
            fi
        fi
        log "Group checks finished!"
    fi

    log "Starting OSP Checks..."
    log "Checking if OSP already exists..."
    if [[ "$listOfAllOSP" =~ "$OSPName" ]]; then
        if [[ "$runType" == "CREATE" ]]; then # We do NOT want to have OSP already created only when creating a new one, otherwise we do want to have it created
            log "ERROR: $OSPName already exists! Skipping row $index..."
            echo "$fullLine" >> $SkippedLinesFile
            continue
        fi
    else
        if [[ "$runType" != "CREATE" ]]; then
            log "ERROR: $OSPName does NOT exist! Skipping row $index..."
            echo "$fullLine" >> $SkippedLinesFile
            continue
        fi
    fi
    log "OSP Check finished!"

    # If runtype is CHECK, end here, marking current row as valid
    if [[ "$runType" == "CHECK" ]]; then
        log "CHECK SUCCESS: Row $index validated successfully!"
        echo "$fullLine" >> $ProcessedLinesFile
        continue
    fi

############# ETL_CL_USER - start #####################
    if [[ "$runType" != "DELETE" && "$runType" != "REMOVE" ]]; then
        log "ETL_CL_USER checks..."
        projectNamePartOne=$(echo "$projectName" | cut -d "_" -f1 )
        projectNamePartTwo=$(echo "$projectName" | cut -d "_" -f3 )
        projectNamePartThree=$(echo "$projectName" | cut -d "_" -f4 )
        condprojectNamePartThree=$( if [[ "$projectNamePartThree" == "00" ]]; then echo ""; else echo "$projectNamePartThree"; fi; )
        ETLCLVarName="USERCL"$projectNamePartOne$projectNamePartTwo$condprojectNamePartThree"NAME"
        ETLCLUserName="ETL_CL_USER_$projectNamePartOne$projectNamePartTwo$projectNamePartThree"
        ETLCLPassVarName="USERCL"$projectNamePartOne$projectNamePartTwo$condprojectNamePartThree"PASS"
        ETLCLPass=""
        # If creating OSP for branch, password will not be generated but rather retrieved from DB from trunk folder-OSP
        if [[ "$isBranch" == "1" ]]; then
            # Using $USERDOMREPOTNSE as TNS connection
            # $USERDOMREPONAME as user
            # $USERDOMREPOPASS as password
            passB64Dec=$( echo "$USERDOMREPOPASS" | base64 --decode )
            trunkOspName="OSP_"$projectNamePartOne"_00_"$projectNamePartTwo"_"$projectNamePartThree
            dbOutput=$( echo "select po.POO_VALUE from infa_dom_mgr.PO_OPTION po inner join infa_dom_mgr.PO_OSPROFILE pos on po.PSO_CONTAINER = pos.PSO_CONTAINER and pos.POO_NAME = '$trunkOspName' where po.POO_NAME like '$ETLCLPassVarName';" | sqlplus -S -M "HTML ON" $USERDOMREPONAME/$passB64Dec@$USERDOMREPOTNSE )
            
            ETLCLPass=$( echo "$dbOutput" | while htmlparse ; do if [[ "$TAG" == "td" ]]; then echo "$VALUE" | tr -d '\n'; fi; done )
            logDev "$dbOutput"
            logDev "$ETLCLPass"
        else
            ETLCLPass=$( openssl rand -base64 25 ) # Generate random password and change it for the ETL_CL_USER
        fi
        ETLCLRunType=""
        logDev "$listOfAllUsers"
        logDev "$ETLCLUserName"
        if [[ "$isBranch" != "1" ]]; then
            if [[ "$listOfAllUsers" =~ "$ETLCLUserName" ]]; then
                ETLCLRunType="ResetPassword -dn $DOMAIN_NAME -ru $ETLCLUserName -rp \"$ETLCLPass\""
            else
                ETLCLRunType="CreateUser -dn $DOMAIN_NAME -nf $ETLCLUserName -nu $ETLCLUserName -np \"$ETLCLPass\""
                listOfAllUsers="$listOfAllUsers $ETLCLUserName"
            fi


            f_infacmdCustom $index "$ETLCLRunType"
            cmdResultCode=$?
            if [[ "$cmdResultCode" != "0" ]]; then
                log "ERROR: Cannot create/resetpassword for user $ETLCLUserName! Row: $index, Username: $ETLCLUserName, ResultCode: $cmdResultCode"
                log "Skipping further processing for row $index..."
                ETLCLPass=""
                echo "$fullLine" >> $SkippedLinesFile
                continue
            fi
            ETLCLPass=$(pmpasswd $ETLCLPass | grep Encrypted | cut -d '>' -f2 | cut -d '<' -f1) # Encrypt password - this will be saved in OSProfile
        fi # <-- Only needed for TRUNK, branches do NOT create ETL_CL user, nor reset its password

        ETLCLFullParameter="'$ETLCLVarName'='$ETLCLUserName' '$ETLCLPassVarName'='$ETLCLPass'" # Contains already encrypted password
        # Add ETL_CL user vars into .profile of given NPA user
        npaProfilePath=/home/infa/.profile
        if [[ "$(cat $npaProfilePath)" =~ "$projectName - ETL_CL START" ]]; then
            oldLine=$(cat $npaProfilePath | grep "$ETLCLPassVarName")
            newLine="export $ETLCLPassVarName=$ETLCLPass"
            sed -i "s/^.*$ETLCLPassVarName.*$/$newLine/gi" $npaProfilePath
        else # Add whole ETL_CL section for given project
            echo "# $projectName - ETL_CL START" >> $npaProfilePath
            echo "export $ETLCLVarName=$ETLCLUserName" >> $npaProfilePath
            echo "export $ETLCLPassVarName=$ETLCLPass" >> $npaProfilePath
            echo "# $projectName - ETL_CL END" >> $npaProfilePath
        fi
#           # PUSHD_00_CZESK_00 - ETL_CL START
#           USERCLPUSHDCZESKNAME=ETL_CL_USER_PUSHDCZESK
#           USERCLPUSHDCZESKPASS=vTqT94oBMUpYbFYgxl72OexGisDoGnv31a9P5hbsHjPeUEGdR8GDxDHKHazUC6dB
#           export USERCLPUSHDCZESKNAME
#           export USERCLPUSHDCZESKPASS
#           # PUSHD_00_CZESK_00 - ETL_CL END
#


        log "ETL_CL_USER checks done!"
    fi
############# ETL_CL_USER - end #####################

    log "Starting OSP Creation..."
    log "Building infacmd OS Profile command..."
    infaCmdSysUsername=$( if [[ "$runType" == "DELETE" || "$runType" == "REMOVE" || "$runType" == "UPDATE" ]]; then echo ""; else echo " -sn $sysUsername"; fi )
    infaCmdEnvVariables=$( if [[ "$runType" == "DELETE" || "$runType" == "REMOVE" ]]; then echo ""; elif [[ "$envVariables" == "$EMPTY_STRING" ]]; then echo " -ev $ETLCLFullParameter"; else echo " -ev $ETLCLFullParameter $envVariables" | sed 's/,/ /g'; fi )
    infaCmdDQEnvVariables=$( if [[ "$DQEnvVariables" == "$EMPTY_STRING" || "$runType" == "DELETE" || "$runType" == "REMOVE" ]]; then echo ""; else echo " -dise $DQEnvVariables" | sed 's/,/ /g'; fi )

    f_infacmdCustom $index "$commandOSPRunType -dn $DOMAIN_NAME -on $OSPName $infaCmdSysUsername $infaCmdEnvVariables $infaCmdDQEnvVariables"
    cmdResultCode=$?
    if [[ "$cmdResultCode" == "0" ]]; then
        log "OS Profile $runType command finished successfully! Row: $index, OSP: $OSPName, ResultCode: $cmdResultCode"
        if [[ "$runType" == "CREATE" ]]; then
            listOfAllOSP="$listOfAllOSP $OSPName"
        elif [[ "$runType" == "DELETE" || "$runType" == "REMOVE" ]]; then
            listOfAllOSP=$(echo $listOfAllOSP | sed "s/$OSPName//g")
        fi     
    else
        log "ERROR: OS Profile $runType command failed! Row: $index, OSP: $OSPName, ResultCode: $cmdResultCode"
        log "Skipping further processing for row $index..."
        echo "$fullLine" >> $SkippedLinesFile
        continue
    fi

    
    if [[ "$runType" != "DELETE" && "$runType" != "REMOVE" ]]; then
        # Assign group permissions to OS Profile - only for CREATE or UPDATE run types
        log "Give ETL_CL user permissions to use OSP..."
        # ETL_CL
        # Give ETL_CL user permission to use OS Profile
        if [[ "$OSPGroup" != "$EMPTY_STRING" ]]; then
            f_infacmdCustom $index "AssignUserPermission -dn $DOMAIN_NAME -on $OSPName -ot OSProfile -eu $ETLCLUserName"
            cmdResult=$?
            if [[ "$cmdResult" != "0" ]]; then
                log "Skipping row $index"
                echo "$fullLine" >> $SkippedLinesFile
                continue
            fi
        fi
        log "Give OSP Groups permissions to use OSP..."
        # Regular group
        # Give IPC Group permission to use OS Profile
        if [[ "$OSPGroup" != "$EMPTY_STRING" ]]; then
            f_infacmdCustom $index "AssignGroupPermission -dn $DOMAIN_NAME -on $OSPName -ot OSProfile -eg $OSPGroup"
            cmdResult=$?
            if [[ "$cmdResult" != "0" ]]; then
                log "Skipping row $index"
                echo "$fullLine" >> $SkippedLinesFile
                continue
            fi
        fi
        # DQ Group
        # Give DQ Group permission to use OS Profile
        if [[ "$DQOSPGroup" != "$EMPTY_STRING" ]]; then
            f_infacmdCustom $index "AssignGroupPermission -dn $DOMAIN_NAME -on $OSPName -ot OSProfile -eg $DQOSPGroup"
            cmdResult=$?
            if [[ "$cmdResult" != "0" ]]; then
                log "Skipping row $index"
                echo "$fullLine" >> $SkippedLinesFile
                continue
            fi
        fi
        log "Group permissions assignment done!"
        #END Assign group permissions block

        # Assign group permissions to given project - only for CREATE or UPDATE run types
        log "Updating repository permissions..."
        # Give IPC Group permission to access project folder
        if [[ "$OSPGroup" != "$EMPTY_STRING" ]]; then
            ipcRead=$( if [[ "$groupRepositoryPermissions" =~ "r" ]]; then echo "r"; else echo ""; fi )
            ipcWrite=$( if [[ "$groupRepositoryPermissions" =~ "w" ]]; then echo "w"; else echo ""; fi )
            ipcExecute=$( if [[ "$groupRepositoryPermissions" =~ "x" ]]; then echo "x"; else echo ""; fi )
            f_pmrepCustom $index "AssignPermission" "-o Folder -n $projectName -g $OSPGroup -p '$ipcRead$ipcWrite$ipcExecute'"
            cmdResult=$?
            if [[ "$cmdResult" != "0" ]]; then
                log "Skipping row $index"
                echo "$fullLine" >> $SkippedLinesFile
                continue
            fi
        fi
        # Give DQ Group permission to access project applications folder
        if [[ "$DQOSPGroup" != "$EMPTY_STRING" ]]; then
            dqRead=$( if [[ "$groupRepositoryPermissions" =~ "r" ]]; then echo "+r"; else echo "-r"; fi )
            dqWrite=$( if [[ "$groupRepositoryPermissions" =~ "w" ]]; then echo "+w"; else echo "-w"; fi )
            dqGrant="-g"
            f_infacmdCustom $index "mrs manageGroupPermissionOnProject -dn $DOMAIN_NAME -sn $MODEL_REP_SVC -pn $projectName -rn $DQOSPGroup -pm \\\"$dqRead$dqWrite$dqGrant\\\""
            cmdResult=$?
            if [[ "$cmdResult" != "0" ]]; then
                log "Skipping row $index"
                echo "$fullLine" >> $SkippedLinesFile
                continue
            fi
        fi
        log "Updating repository permissions done!"
        
        # Apply OS Profile to given project - only for CREATE or UPDATE run types
        log "Apply $OSPName to project $projectName"
        f_pmrepCustom $index "modifyFolder" "-n $projectName -u $OSPName" 
        cmdResult=$?
        if [[ "$cmdResult" != "0" ]]; then
            log "Skipping row $index"
            echo "$fullLine" >> $SkippedLinesFile
            continue
        fi

        # Add ETL_CL_USER to ETL_CL group - TBD: Is it needed?

        #  Add permissions for ETL_CL_USER to project - read, execute rights
        log "Adding read/execute permissions for $ETLCLUserName to project $projectName"
        f_pmrepCustom $index "AssignPermission" "-o Folder -n $projectName -u $ETLCLUserName -p 'rx'"
        cmdResult=$?
        if [[ "$cmdResult" != "0" ]]; then
            log "Skipping row $index"
            echo "$fullLine" >> $SkippedLinesFile
            continue
        fi

        #  Add permissions for ETL_CL_USER to connections - read, execute rights
        log "Going through all connections..."
        echo $listOfAllConnections | while read line; do
            conName=$(echo $line | cut -d ',' -f1)
            conType=$(echo $line | cut -d ',' -f2)
            if [[ "$conName" =~ "$projectNamePartOne$projectNamePartTwo" ]]; then
                logDev "Adding read/execute permissions for $ETLCLUserName to connection $conName of $conType type"
                f_pmrepCustom $index "AssignPermission" "-o Connection -t $conType -n $conName -u $ETLCLUserName -p 'rx'"
                cmdResult=$?
                if [[ "$cmdResult" != "0" ]]; then
                    log "Skipping row $index"
                    echo "$fullLine" >> $SkippedLinesFile
                    continue
                fi
            fi
        done

    fi 
    
    log "SUCCESS: Row $index processed successfully!"
    echo "$fullLine" >> $ProcessedLinesFile

done

log "All rows were processed!"
log "Terminating..."
