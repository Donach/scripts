#!/bin/bash
#
#Name:           Planning_Workflows.sh
#Description:    Take input parameters from supplied file, performing Schedule, Unschedule, List or Trigger action of workflows.
#Author:         Daniel Havlicek
#Date:           14/02/2019
#Changed:        23/09/2020 - Daniel Havlicek
#Changelog:      23/09/2020 - Daniel Havlicek
#					 - Added few checks, personalization of the script for project 33

. ~/.profile

#/opt/Informatica/infa_shared/scripts/PIRBN_00_CSHMN_00/Planning_Workflows.sh $PMIntegrationServiceName $PMDomainName /opt/Informatica/infa_shared/scripts/PIRBN_00_CSHMN_00/Input_PIRBN_00_CSHMN_00.csv /opt/Informatica/infa_shared/WorkflowLogs/PIRBN_00_CSHMN_00 Y
#sh -c "/opt/Informatica/infa_shared/scripts/PSF91/Planning_Workflows.sh devis $PMDomainName /opt/Informatica/infa_shared/TgtFiles/PSF91/wf_Schedule_CHECK/wf_to_be_scheduled.csv /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK N"
echo_time() {
    date +"%H:%M:%S - $(echo -e "$@" | sed 's/%/%%/g')"
}

Infa_IS=$1
Infa_Domain=$2
Input_CSV=$3
Log_Path=$4
Flag_RunMode=$5 # S - schedule; U - unschedule; L - list; T - trigger
Flag_Force=$6 # Y - Start workflow even if previous instance is still running; N - wait 60 seconds and try to start workflow again
Delay=$7 # Delay after which the trigger will run, only applicable when >= 0
WfRunId=$8

OutputResult=$Input_CSV".result"
if [[ "$Delay" == "" ]]; then
	Delay=5
fi


currDate=$(date +%c)
LOG_FILE="$Log_Path/Planning_Workflows_"$currDate"_$Flag_RunMode.log"
triggerMutexFile="$Log_Path/triggerMutex_$Delay"
triggerMutexTime=0
triggerMutexReady=0

echo_time "$currDate"
#- Disabled for NDPAS project

#Debugs
#echo_time "$LOG_FILE" >> $LOG_FILE
#echo_time "$LOG_FILE" >> /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK/test.txt
#chmod 777 /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK/test.txt
#touch $LOG_FILE
#if [[ $Infa_Domain == "devdom" ]]; then
#	chmod -R 777 $LOG_FILE
#	chmod -R 777 $Log_Path
#fi


#All outputs are redirect to the logfile
# exec >> $LOG_FILE #- Disabled for NDPAS project

# debugg - all input parametters
if [[ $Infa_Domain == "devdom" ]]; then echo_time "Run Params:\n$@"; fi

#create arrays from CSV files with comma separator
arrFolder=( $(cut -d ';' -f1 $Input_CSV ) )
arrWorkflow=( $(cut -d ';' -f2 $Input_CSV ) )
arrCL_User=( $(cut -d ';' -f3 $Input_CSV ) )
arrEnv_Pass=( $(cut -d ';' -f4 $Input_CSV ) )
passEnv=""
pmrunmode=""
pmdetails=""
procFolders=""
errCount=0
okCount=0

# If RunMode is "L" run check to list all scheduled workflows, else run un/schedule procedure
case "$Flag_RunMode" in
	"L")
		pmrunmode="getservicedetails"
		;;
	"LA")
		pmrunmode="getservicedetails"
		;;
	"U")
		pmrunmode="unscheduleworkflow"
		;;
	"S")
		pmrunmode="scheduleworkflow"
		;;
	"T")
		pmrunmode="startworkflow"
		;;
	"STOP")
		pmrunmode="stopworkflow"
		;;
	*)
		pmrunmode=""
		;;
esac
if [[ $pmrunmode == "" ]]; then
	echo_time "RunMode is not properly defined! (5th parameter)"
	echo_time "Terminating..."
	exit 1
fi
if [[ -f $OutputResult ]]; then
	rm $OutputResult
fi

# Do for all array elements
for (( index = 0; index < ${#arrFolder[@]}; index++ )) # TODO: Find out how this works exactly
do 
	folder="${arrFolder[$index]}"
	if [[ "$Infa_Domain" == "devdom" || "$Infa_Domain" == "tstdom" ]]; then
		if [[ "${arrWorkflow[$index]}" == "wf_22_APF_FULL" || "${arrWorkflow[$index]}" == "wf_22_MPF_LOAD_TABLES" ]]; then
			folder=`echo "$folder" | sed -e 's/_00_/_22_/g'`
		elif [[ "${arrWorkflow[$index]}" == "wf_33_mpoint_00_Master_MP" ]]; then
			folder=`echo "$folder" | sed -e 's/_00_/_33_/g'`
		else
			folder=`echo "$folder" | sed -e 's/_00_/_11_/g'`
		fi
	fi
	userName="${arrCL_User[$index]}"
	passEnv="${arrEnv_Pass[$index]}"
	workflow="${arrWorkflow[$index]}"
	echo_time "\nProcessing $folder - Worfklow $workflow"
	# Check for RunMode, if its just to "L"ist scheduled workflows, we want to lookup the schedule for each folder only once.
	if [[ "$Flag_RunMode" =~  "L" && $procFolders != *"$folder"* ]]; then  
		procFolders="$procFolders;$folder"
		if [[ "$Flag_RunMode" ==  "L" ]]; then
			pmdetails="-scheduled | grep 'Workflow:' | cut -d'[' -f 2 | cut -d']' -f 1" 
		else
			pmdetails="-all" 
		fi
	elif [[ ! "$Flag_RunMode" =~  "L" ]]; then
		if [[ "$WfRunId" != "" ]]; then
			pmdetails="-f $folder -wfrunid $WfRunId $workflow"
		else
			pmdetails="-f $folder $workflow"
		fi
	else # If RunMode is "L" but the current folder was already processed, skip to next array member
		continue
	fi
	
	# Build the PMCMD command using common params and specific pmdetails based on RunMode
	pmcommand="pmcmd $pmrunmode -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails"

	# Triggering only - check if the workflow is currently running
	if [[ "$Flag_RunMode" == "T" || "$Flag_RunMode" == "STOP" ]]; then
		wfCurrentStatus=$( pmcmd getworkflowdetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails | grep 'Workflow run status' | cut -d '[' -f2 | cut -d ']' -f1 )
		echo_time "Current workflow status: $wfCurrentStatus"
		if [[ "$wfCurrentStatus" == "Running" ]]; then
			# Check if delay is bigger than 5 minutes
			if [[ $Delay -gt 300 ]]; then
				# Check if mutex file exists, if yes, current process has to wait a bit more before starting a workflow
				if [[ ! -f $triggerMutexFile ]]; then
					touch $triggerMutexFile
					triggerMutexTime=$((`date +%s`+$Delay))
					echo "$triggerMutexTime" > $triggerMutexFile
				else
					if [[ `cat $triggerMutexFile` -gt 0 ]]; then
						triggerMutexTime=`cat $triggerMutexFile`
						ElapsedTime=$((`cat $triggerMutexFile`-`date +%s`+$Delay))
					else # should not ever happen
						Delay=$Delay+$Delay
						triggerMutexTime$((`date +%s`+$Delay))
						echo "$triggerMutexTime" > $triggerMutexFile
					fi
				fi
			fi
			
			echo_time "Waiting $Delay seconds to check the workflow status and do further action" && sleep $Delay
			if [[ $Delay -gt 300 ]]; then
				while [ ! $triggerMutexReady ]; do
					if [[ -f $triggerMutexFile ]]; then
						if [[ $((`cat $triggerMutexFile`-`date +%s`)) -le 0 || $ElapsedTime -le 0 ]]; then
							rm $triggerMutexFile
							triggerMutexReady=1
						else
							sleep 5
						fi
					else
						echo "$triggerMutexTime" > $triggerMutexFile
						sleep 5
					fi
				done
			fi
			
			wfCurrentStatus=$( pmcmd getworkflowdetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails | grep 'Workflow run status' | cut -d '[' -f2 | cut -d ']' -f1 )
			
			if [[ "$Flag_RunMode" == "T" && "$Flag_Force" != "Y" ]]; then
				if [[ "$wfCurrentStatus" == "Running" ]]; then
					echo_time "Cannot start workflow $workflow, it is currently running!"
					echo_time "$folder - $workflow - ERROR - Workflow is running" >> $OutputResult
					(( errCount += 1 ))
					continue
				else
					echo_time "Workflow is not running, going to start it now..."
				fi
			elif [[ "$Flag_RunMode" == "T" && "$Flag_Force" == "Y" ]]; then
				echo_time "Going to start another workflow instance with the already running one..."
			else # If Flag_RunMode == "STOP"
				if [[ "$wfCurrentStatus" == "Running" ]]; then
					abort=0
					echo_time "Checking if workflow running longer than wanted"
					for (( i=0; i <= 4; i++ )); do
						wfStartDate=$( pmcmd getworkflowdetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails | grep 'Start time:' | cut -d '[' -f2 | cut -d ']' -f1 )
						wfRuntime=$((`date +%s`-`date -d "$wfStartDate" +%s`))
						if [[ $wfRuntime -gt $Delay ]]; then
							echo_time "Workflow is running, going to stop it now..."
							abort=0
							break
						else
							abort=1
						fi
						sleep 10
					done
					if [[ $abort == 1 ]]; then
						echo_time "Workflow will NOT be stopped..."
						continue
					fi
				else
					echo_time "Workflow is not running, no need to stop it..."
					continue
				fi
			fi
		else
			if [[ "$Flag_RunMode" == "T" ]]; then
				echo_time "Workflow is not running, going to start it now..."
				if [[ $Delay -ge 0 ]]; then # Greater or Equal to 0
					echo_time "Waiting $Delay seconds to start the workflow" && sleep $Delay
				fi
			else # If Flag_RunMode == "STOP"
				echo_time "Workflow is not running, no need to stop it..."
				continue
			fi
		fi
	fi

	## - This might be necessary, keep this code around!
	#export $passEnv # TODO: Has to be run to load ENV variable properly ?

	# Run the actual PMCMD command and save its output to variable. Save also its result code for later check
	resultCode=0
	result=$(eval $pmcommand; resultCode=$?;) # dynamically generated command based on RunMode flag

	# Based on RunMode, process the result from previous command if the result code is 0
	if [[ $resultCode != 0 || $result =~ "ERROR" ]]; then
		echo_time "ERROR: !!!!script The process has failed!!!!! "
		echo_time "Code: $resultCode"
		echo_time "Domain: $Infa_Domain; Service: $Infa_IS"
		echo_time "Could not $pmrunmode ${arrWorkflow[$index]} in folder ${arrFolder[$index]} using ${arrCL_User[$index]} as user and $passEnv and name of ENV password."
		(( errCount += 1 ))
		echo_time "Result: $result"
		# exit 1
	else
		if [[ "$Flag_RunMode" ==  "L" ]]; then
			# Take the results and format them so they will be read as lines by the while loop, prefixing each workflow with its foldername and putting it to temporary list of scheduled workflows
			echo_time $result | cut -f 1 | tr ' ' '\n'  | while read line; do echo_time "$folder;$line;" >> $OutputResult; done
			if [[ ! -f $OutputResult ]]; then
				echo_time "ERROR: Could not create file with list of scheduled workflows! - $OutputResult"
				resultCode=255
			else
				echo_time "SUCCESS: List of scheduled workflows was created: $OutputResult"
			fi
		elif [[ "$Flag_RunMode" ==  "LA" ]]; then
			echo_time $result
		elif [[ "$Flag_RunMode" ==  "STOP" ]]; then
			sleep 1800
			wfCurrentStatus=$( pmcmd getworkflowdetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails | grep 'Workflow run status' | cut -d '[' -f2 | cut -d ']' -f1 )
			
			if [[ "$wfCurrentStatus" == "Stopping" || "$wfCurrentStatus" == "Running" ]]; then
				echo_time "Workflow is still in state Stopping or Running. Going to abort it now!"
				pmcommand="pmcmd abortworkflow -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails"
				
				resultCode=0
				result=$(eval $pmcommand; resultCode=$?;) # dynamically generated command based on RunMode flag
				if [[ $resultCode != 0 || $result =~ "ERROR" ]]; then
					echo_time "ERROR: !!!!script The process has failed!!!!! "
					echo_time "Code: $resultCode"
					echo_time "Domain: $Infa_Domain; Service: $Infa_IS"
					echo_time "Could not $pmrunmode ${arrWorkflow[$index]} in folder ${arrFolder[$index]} using ${arrCL_User[$index]} as user and $passEnv and name of ENV password."
					(( errCount += 1 ))
					echo_time "Result: $result"
				else
					# INFO
					checkInfo=$(echo_time $result | grep $workflow | grep 'INFO')
					#echo_time "DEBUG INFO:"$checkInfo
					# ERROR
					checkError=$(echo_time $result | grep $workflow | grep 'ERROR')
					#echo_time "DEBUG ERROR:"$checkError
					if [[ $checkError == "" ]]; then
						echo_time "$folder - $workflow ---> OK" >> $OutputResult
						(( okCount += 1 ))
					else
						echo_time "$folder - $workflow ---> ERROR" >> $OutputResult
						(( errCount += 1 ))
					fi

					echo_time "Results: $result"  | grep $workflow
				fi
			fi
		else
			# INFO
			checkInfo=$(echo_time $result | grep $workflow | grep 'INFO')
			#echo_time "DEBUG INFO:"$checkInfo
			# ERROR
			checkError=$(echo_time $result | grep $workflow | grep 'ERROR')
			#echo_time "DEBUG ERROR:"$checkError
			if [[ $checkError == "" ]]; then
				echo_time "$folder - $workflow ---> OK" >> $OutputResult
				(( okCount += 1 ))
			else
				echo_time "$folder - $workflow ---> ERROR" >> $OutputResult
				(( errCount += 1 ))
			fi

			echo_time "Results: $result"  | grep $workflow
		fi

	fi
done


echo_time "\n\nThe process has finished successfully"
echo_time "Successfully scheduled workflows: $okCount"
echo_time "Total number of errors: $errCount"

echo_time "\nSuccessfully scheduled workflows: $okCount" >> $OutputResult
echo_time "Total number of errors: $errCount" >> $OutputResult
echo_time "Exit code: $resultCode \n"

exit $resultCode
