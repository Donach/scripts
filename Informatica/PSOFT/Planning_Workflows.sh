#!/bin/bash
#
#Name:           Planning_Workflows.sh
#Description:    Take input parameters from supplied file, performing Schedule, Unschedule, List or Trigger action of workflows.
#Author:         Daniel Havlicek
#Date:           14/02/2019
#Changed:        23/09/2020 - Daniel Havlicek
#Changelog:      23/09/2020 - Daniel Havlicek
#					 - Added few checks, personalization of the script for project 33
#                08/08/2022 - Daniel Havlicek
#                   - 

. ~/.profile
. /home/$(whoami)/.profile

#/opt/Informatica/infa_shared/scripts/PIRBN_00_CSHMN_00/Planning_Workflows.sh $PMIntegrationServiceName $PMDomainName /opt/Informatica/infa_shared/scripts/PIRBN_00_CSHMN_00/Input_PIRBN_00_CSHMN_00.csv /opt/Informatica/infa_shared/WorkflowLogs/PIRBN_00_CSHMN_00 Y
#sh -c "/opt/Informatica/infa_shared/scripts/PSF91/Planning_Workflows.sh devis $PMDomainName /opt/Informatica/infa_shared/TgtFiles/PSF91/wf_Schedule_CHECK/wf_to_be_scheduled.csv /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK N"

Infa_IS=$1
Infa_Domain=$2
Input_CSV=$3
Log_Path=$4
Flag_RunMode=$5 # S - schedule; U - unschedule; L - list; T - trigger
Infa_Folder=$6 # Only needed for "L" RunMode
Infa_WfName=$7 # wf_Schedule_CHECK; Only needed for "L" RunMode

OutputResult=$Input_CSV".result"


currDate=$(date +%Y%m%d_%H%M%S)
LOG_FILE="$Log_Path/Planning_Workflows_"$currDate"_$Flag_RunMode.log"

#Debugs
#echo "$LOG_FILE" >> $LOG_FILE
#echo "$LOG_FILE" >> /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK/test.txt
#chmod 777 /opt/Informatica/infa_shared/WorkflowLogs/PSF91/wf_Schedule_CHECK/test.txt
if [[ $Infa_Domain == "devdom" ]]; then
	#chmod -R 777 $LOG_FILE
	chmod -R 777 $Log_Path
fi

#All outputs are redirect to the logfile
exec >> $LOG_FILE

# debugg - all input parametters
if [[ $Infa_Domain == "devdom" ]]; then echo -e "Run Params:\n$@\n\n"; fi

#create arrays from CSV files with comma separator
arrFolder=( $(cut -d ';' -f1 $Input_CSV ) )
arrWorkflow=( $(cut -d ';' -f2 $Input_CSV ) )
arrCL_User=( $(cut -d ';' -f3 $Input_CSV ) )
arrEnv_Pass=( $(cut -d ';' -f4 $Input_CSV ) )
passEnv=""
pmrunmode=""
pmdetails=""
procFolders=""
errCount=0
okCount=0

# If RunMode is "L" run check to list all scheduled workflows, else run un/schedule procedure
case "$Flag_RunMode" in
	"L")
		pmrunmode="getservicedetails"
		;;
	"U")
		pmrunmode="unscheduleworkflow"
		;;
	"S")
		pmrunmode="scheduleworkflow"
		;;
	"T")
		pmrunmode="startworkflow"
		;;
	*)
		pmrunmode=""
		;;
esac
if [[ $pmrunmode == "" ]]; then
	echo "RunMode is not properly defined! (5th parameter)"
	echo "Terminating..."
	exit 1
fi
if [[ -f $OutputResult ]]; then
	rm $OutputResult
fi

# Do for all array elements
for (( index = 0; index < ${#arrFolder[@]}; index++ )) # TODO: Find out how this works exactly
do 
	folder="${arrFolder[$index]}"
	userName="${arrCL_User[$index]}"
	passEnv="${arrEnv_Pass[$index]}"
	workflow="${arrWorkflow[$index]}"
	echo -e "\nProcessing $folder - Worfklow $workflow"
	# Check for RunMode, if its just to "L"ist scheduled workflows, we want to lookup the schedule for each folder only once.
	if [[ "$Flag_RunMode" ==  "L" && $procFolders != *"$folder"* ]]; then  
		procFolders="$procFolders;$folder"
		pmdetails="-scheduled | grep 'Workflow:' | cut -d'[' -f 2 | cut -d']' -f 1" 
	elif [[ "$Flag_RunMode" !=  "L" ]]; then
		pmdetails="-f $folder $workflow"
	else # If RunMode is "L" but the current folder was already processed, skip to next array member
		continue
	fi
	# Build the PMCMD command using common params and specific pmdetails based on RunMode
	pmcommand="pmcmd $pmrunmode -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails"

	# Triggering only - check if the workflow is currently running
	if [[ "$Flag_RunMode" == "T" ]]; then
		wfCurrentStatus=$( pmcmd getworkflowdetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv $pmdetails | grep 'Workflow run status' | cut -d '[' -f2 | cut -d ']' -f1 )
		echo "Current workflow status: $wfCurrentStatus"
		if [[ "$wfCurrentStatus" == "Running" ]]; then
			echo "Cannot start workflow $workflow, it is currently running!"
			echo "$folder - $workflow - ERROR - Workflow is running" >> $OutputResult
			(( errCount += 1 ))
			continue
		else
			echo "Workflow is not running, going to start it now..."
		fi
	fi

	## - This might be necessary, keep this code around!
	#export $passEnv # TODO: Has to be run to load ENV variable properly ?

	# Run the actual PMCMD command and save its output to variable. Save also its result code for later check
	resultCode=0
	result=$(eval $pmcommand; resultCode=$?;) # dynamically generated command based on RunMode flag

	# Based on RunMode, process the result from previous command if the result code is 0
    # echo "#0"  >> $OutputResult
	if [[ $resultCode != 0 || $result =~ "ERROR" ]]; then
        # echo "#1"  >> $OutputResult
		echo "ERROR: !!!!script The process has failed!!!!! "
		echo "Code: $resultCode"
		echo "Domain: $Infa_Domain; Service: $Infa_IS"
		echo "Could not $pmrunmode ${arrWorkflow[$index]} in folder ${arrFolder[$index]} using ${arrCL_User[$index]} as user and $passEnv and name of ENV password."
		(( errCount += 1 ))
		echo "Result: $result"
		# exit 1
	else
        # echo "#2"  >> $OutputResult
		if [[ "$Flag_RunMode" ==  "L" ]]; then
            # echo "#3"  >> $OutputResult
			# Take the results and format them so they will be read as lines by the while loop, prefixing each workflow with its foldername and putting it to temporary list of scheduled workflows
			echo $result | cut -f 1 | tr ' ' '\n'  | while read line; do echo "$folder;$line;" >> $OutputResult; done
			if [[ ! -f $OutputResult ]]; then
				echo "ERROR: Could not create file with list of scheduled workflows! - $OutputResult"
				resultCode=255
			else
				echo "SUCCESS: List of scheduled workflows was created: $OutputResult"
			fi
		else
            # echo "#4"  >> $OutputResult
			# INFO
			checkInfo=$(echo $result | grep $workflow | grep 'INFO')
			#echo "DEBUG INFO:"$checkInfo
			# ERROR
			checkError=$(echo $result | grep $workflow | grep 'ERROR')
			#echo "DEBUG ERROR:"$checkError
            # Check if workflow is running for over 24h
            checkLongrun=$( pmcmd getservicedetails -sv $Infa_IS -d $Infa_Domain -u $userName -pv $passEnv | grep "$workflow" -A 4 | grep "Workflow run status: \[Running" -A 3 -B 1 )
            # echo "DEBUG $workflow checkLongrun:"$checkLongrun >> $OutputResult
            if [[ "$checkLongrun" != "" && "$checkLongrun" =~ "[Running]" ]]; then
                # echo "#5"  >> $OutputResult
                checkLongrunWfRunStatus=$( echo "$checkLongrun" | grep "run status" | cut -d'[' -f2 | cut -d']' -f1 )
                #echo "DEBUG $workflow checkLongrunWfRunStatus:"$checkLongrunWfRunStatus >> $OutputResult
                checkLongrunStartTime=$( echo "$checkLongrun" | grep "Start time" | cut -d'[' -f2 | cut -d']' -f1 )
                checkLongrunStartTimeTrunc=$(echo "$checkLongrunStartTime" | cut -d' ' -f2,3,4,5)
                #echo "DEBUG $workflow checkLongrunStartTime:"$checkLongrunStartTime"|||" >> $OutputResult
                #echo "DEBUG $workflow checkLongrunStartTimeTrunc:"$checkLongrunStartTimeTrunc >> $OutputResult
                # Calculate second difference between curent date and rundate
                checkLongrunWfRuntime=`echo "$(date '+%s')""-""$( date -d "$checkLongrunStartTimeTrunc" '+%s' )" | bc`
                #echo "DEBUG $workflow checkLongrunWfRuntime:"$checkLongrunWfRuntime >> $OutputResult
                # If WF runtime is greater than 24h, throw an error
                if [[ $checkLongrunWfRuntime -ge 86400 ]]; then
                    echo -e "$folder - $workflow ---> ERROR - Workflow runs longer than 24 hours! \n" >> $OutputResult
                    (( errCount += 1 ))
                fi
            else
                # echo "#6"  >> $OutputResult
                if [[ $checkError == "" ]]; then
                    echo -e "$folder - $workflow ---> OK\n" >> $OutputResult
                    (( okCount += 1 ))
                else
                    echo -e "$folder - $workflow ---> ERROR\n" >> $OutputResult
                    (( errCount += 1 ))
                fi
            fi

			echo "Results: $result"  | grep $workflow
		fi

	fi
done
# echo "#7"  >> $OutputResult

echo -e "\n\nThe process has finished successfully"
echo "Successfully scheduled workflows: $okCount"
echo "Total number of errors: $errCount"

echo -e "\n\nSuccessfully scheduled workflows: $okCount" >> $OutputResult
echo -e "Total number of errors: $errCount" >> $OutputResult
echo "Exit code: "$resultCode

exit $resultCode
