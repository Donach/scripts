#!/bin/bash
# Created by: Daniel Havlicek
# Date: 05.04.2019
# Description: This script will zip all folders given in param BatchType into single TGZ file and send it over to given SFTP server.
# Changelog: 
#
#
# First parameter is full path to where does the workflow save target files
PMTargetDir=$1
# Batch Date
DATE=$2
# Start date of the batch (DATUM_OD)
StartDate=$3
# End date of the batch (DATUM_DO)
EndDate=$4
# Batch type, can be "D", "Q", "Y" or "D Q Y"
BatchType=$5
# Name of connection of SFTP server eg. infa_act_cz 
SFTPServer=$6


#DATE=$(date +%Y%m%d_%H%M)
FPREFIX="PMDATA_"$StartDate"-"$EndDate"_Result"
DIRNAME=$FPREFIX"_"$DATE
FNAME=$FPREFIX"_"$DATE".tgz"


if [[ -z $PMTargetDir ]]; then
	echo "Parameter PMTargetDir is not set! Terminating..."
	exit 1
fi

if [[ -z $StartDate ]]; then
	echo "Parameter StartDate is not set! Terminating..."
	exit 2
fi

if [[ -z $EndDate ]]; then
	echo "Parameter EndDate is not set! Terminating..."
	exit 3
fi

if [[ -z $BatchType ]]; then
	echo "Parameter BatchType is not set! You must use D, Q, Y or 'D Q Y'. Terminating..."
	exit 4
elif [[ ! $BatchType =~ "D" && ! $BatchType =~ "Q" && ! $BatchType =~ "Y" ]]; then  #|| ! $BatchType == "D Q Y" ]]; then
	echo "BatchType is not set properly! You must use D, Q, Y or 'D Q Y'. Terminating..."
	exit 4
fi

if [[ -z $SFTPServer ]]; then
	echo "Target SFTP server connection name is not set! Terminating..."
	exit 5
fi


#D=(! -d $PMTargetDir"/D" && ($BatchType -eq "D" || $BatchType -eq "D Q Y"))
#Q=(! -d $PMTargetDir"/Q" && ($BatchType -eq "Q" || $BatchType -eq "D Q Y"))
#Y=(! -d $PMTargetDir"/Y" && ($BatchType -eq "Y" || $BatchType -eq "D Q Y"))
if [[ ! -d $PMTargetDir"/D" && $BatchType =~ "D" || ! -d $PMTargetDir"/Q" && $BatchType =~ "Q" || ! -d $PMTargetDir"/Y" && $BatchType =~ "Y" ]]; then
	echo "One of batches was not generated properly or PMTargetDir parameter is not set properly!"
	exit 6
fi

echo "All parameters set properly:"
echo "PMTargetDir: $PMTargetDir"
echo "DATE: $DATE"
echo "StartDate: $StartDate"
echo "EndDate: $EndDate"
echo "BatchType: $BatchType"
echo "SFTPServer: $SFTPServer"

# Go to target folder and zip the batches
cd $PMTargetDir
echo "Creating archive $FNAME"
tar -zcf $FNAME $BatchType

# Open connection with server
sftp $SFTPServer <<EOF
	mkdir $DIRNAME
	cd $DIRNAME
	put $FNAME
	bye
EOF

TRANSFERRESULT=$?

if [[ $TRANSFERRESULT -ne 0 ]]; then
	echo "Could not upload archive $FNAME to $SFTPServer! Error code: $TRANSFERRESULT"\n"Terminating..."
	exit $TRANSFERRESULT
fi


echo "Archive transferred successfully!"
exit 0