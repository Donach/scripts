"""
Name:           FieldPrecision.py
Description:    This script goes through supplied XML and updates precision on all fields from given CSV file.
Author:         Daniel Havlicek
Date:           23/07/2018
Changed:        23/07/2018 - Daniel Havlicek
Changelog:      23/07/2018 - Daniel Havlicek
					- Added 4th input parameter - TRUE/FALSE - force update Targets with new precision values
                24/07/2018 - Daniel Havlicek
                    - Added more comments, each functions has brief description. Fixed bugs.

"""


import xml.etree.ElementTree as ET
import sys # Imports package for handling arguments
import os #Creation of directories, other system access
import re # Regular Expressions
from pathlib import Path

DEBUG_ON = False

severityInfo    = "INFO"
severityWarning = "WARNING"
severityError   = "ERROR"
severityFatal   = "FATAL"
ENCODING        = "utf-8"
# TEMPORARY
if (len(sys.argv) == 1):
    #sys.argv.append("c:\\Users\\QU88DR\\Documents\\Work\\NN\\FieldPrecisionChange\\INPUT\\PSOFT_00_HYPER_00.xml")
    sys.argv.append("c:\\Users\\QU88DR\\Documents\\Work\\NN\\FieldPrecisionChange\\INPUT\\DWHPL_00_STAGE_00.xml")
    sys.argv.append("c:\\Users\\QU88DR\\Documents\\Work\\NN\\FieldPrecisionChange\\OUTPUT")
    sys.argv.append("c:\\Users\\QU88DR\\Documents\\Work\\NN\\FieldPrecisionChange\\INPUT\\changedFields.csv")
    sys.argv.append("TRUE")
# END TEMPORARY
    

# COLUMN_ID: Which column will be used as a key and which column should be updated in the XML
# INPUT_FILE: Full path to input file
# OUTPUT_DIRECTORY: Path to output directory - Do NOT specify filename
# FILE_OF_CHANGES: CSV file with requested changes to be done in the XML¨
# FORCE_UPDATE_TARGET: "TRUE" if you want to force update on TARGET objects, even if they are not used/no connections are leading to the corresponding field.
print("Arguments: OK") if len(sys.argv) == 5 else sys.exit("Wrong number of arguments, expecting 5 - COLUMN_ID, INPUT_FILE, OUTPUT_DIRECTORY, FILE_OF_CHANGES, FORCE_UPDATE_TARGET")
# Initialize variables
xmlParserRoot = None
xmlParser = None
inputFile  = Path(sys.argv[1])
outputDirectory = Path(sys.argv[2])
# File in CSV format containing requested changes
# Format: 
changesFile = Path(sys.argv[3]) 
# Switch to update TARGET definitions even if there is no connection leading to the field
forceUpdateTarget = False
if (sys.argv[4] == "TRUE" or sys.argv[4] == "1" or sys.argv[4] == 1):
    forceUpdateTarget = True

#Global definitions
folderName = None
mappings = None
reuseTransforms = None
targets = None
sources = None
separator = None
#Local definitions/instances
mappingName = ""
transformations = []
sourceInstances = None
targetInstances = None
transformInstances = None
listOfFields = []
listOfChangedFields = []
fieldMap = {}
connectors  = None
isTargetDef = False
sourceFields = None
targetFields = None


# END Initialize variables

if (not (outputDirectory.is_dir())):
    print("Creating missing directories...")
    os.makedirs(outputDirectory)

##################################
##          Functions           ##
##################################
def getSeparator(line):
    """
    Calculates separator based on the inserted line
    Takes: string line
    Returns: string message
    """
    global separator
    countSepSemicolon = line.count(';')   # ;
    countSepComma = line.count(',')       # ,
    countSepPipe = line.count('|')   	  # |
    if separator:
        if countSepSemicolon > 0 and separator == ";":
            return "Separator found: Semicolon (;)"
        elif countSepComma > 0 and separator == ",":
            return "Separator found: Comma (,)"
        elif countSepPipe > 0 and separator == "|":
            return "Separator found: Pipe (|)"
        else:
            sys.exit("Could not find separator. Please use either Semicolon (;), Comma (,) or Pipe (|).")
    if countSepSemicolon > 0 and countSepSemicolon > countSepComma and countSepSemicolon > countSepPipe:
        separator = ";"
    elif countSepComma > 0 and countSepComma > countSepSemicolon and countSepComma > countSepPipe:
        separator = ","
    elif countSepPipe > 0 and countSepPipe > countSepComma and countSepPipe > countSepSemicolon:
        separator = "|"
    else:
        separator = "NOT FOUND"
        return "Separator NOT FOUND!"

def processInput():
    """ Main processing function. Runs all other necessary functions. \n
    Takes the global variables, which are filled from the parameters when running the script.

    """
    basename = os.path.basename(inputFile) # Name of currently processed folder
    global outputDirectory
    global listOfFields
    global fieldMap
    global changesFile
    global xmlParserRoot
    global separator
    global listOfChangedFields
    # Go through changesFile, putting each line into array and that array into hashmap. add 2nd element into hashmap as key
    with changesFile.open('r') as openFile:
        line = openFile.readline()
        getSeparator(line)
        line = openFile.readline()
        print(getSeparator(line))
        while line:
            array = line.replace("\n","").split(separator)

            if len(array) > 1 and not fieldMap.get(array[1]):
                listOfFields.append(array[1])
                fieldMap.update({array[1] : array})
            line = openFile.readline()

    # define file which will be written into
    outputFile = outputDirectory / (basename )
    # create file if it does nto exist
    outputFile.exists() or outputFile.touch() #sys.exit("Cannot open file "+outputFile)


    if (Path(inputFile).exists):
        print("\t File: "+inputFile.name)
        # Process the XML file - need to get full path
        processFile(inputFile)
    
    # Write the new tree to the file
    print("Writing to file: ", outputFile)
    xmlParser.write(outputFile)
    print("Success!")
    print("List of changed fields:")
    print(listOfChangedFields)
    print("Terminating...")

def processFile(inputFile):
    """Go through each POWERMART and each Repository inside it, and submit all Folders for processing. \n
    Takes: string inputFile (path to a file on filesystem)
    """
    global xmlParserRoot
    global xmlParser
    global folderName
    global mappings
    global reuseTransforms
    global targets
    global sources
    xmlParser = ET.parse(inputFile)
    xmlParserRoot = xmlParser.getroot()

    
    
    for POWERMART in xmlParser.iter('POWERMART'):
        print(2*"\t"+str(POWERMART.tag))
        for REPOSITORY in POWERMART.iter('REPOSITORY'):
            print(3*"\t"+str(REPOSITORY.tag)+": "+REPOSITORY.attrib.get('NAME'))
            #print(REPOSITORY.attrib.get('NAME'))
            for FOLDER in REPOSITORY.iter('FOLDER'):
                print(4*"\t"+FOLDER.tag+": "+FOLDER.attrib.get('NAME'))
                #print(FOLDER.attrib.get('NAME'))
                folderName      =   FOLDER.attrib.get('NAME')
                mappings        =   FOLDER.findall('MAPPING')
                mappings       +=   FOLDER.findall('MAPPLET')
                reuseTransforms =   FOLDER.findall('TRANSFORMATION')
                targets         =   FOLDER.findall('TARGET')
                sources         =   FOLDER.findall('SOURCE')
                for mapping in mappings:
                    print(5*"\t"+mapping.tag+": "+mapping.attrib.get('NAME'))
                    #print(5*"\t",mapping.find('../../'))
                    processMapping(folderName, mapping, sources, reuseTransforms, targets)

# Processing for Mappings and Mapplets
def processMapping(folderName, mappingElement, sources, reuseTransforms, targets):
    """ Gets all necessary data from the Mapping and then processes each instance of any Source Object. \n
    Takes: string folderName, XMLElement mappingElement, XMLElementList sources, XMLElementList reuseTransforms, XMLElementList targets
    """
    global mappingName
    mappingName = mappingElement.attrib.get('NAME')
    global transformations
    transformations = mappingElement.findall('TRANSFORMATION')                      # Mapping specific
    global sourceInstances
    sourceInstances = mappingElement.findall('INSTANCE[@TYPE="SOURCE"]')            # Instances of reusable source objects
    global transformInstances
    transformInstances = mappingElement.findall('INSTANCE[@TYPE="TRANSFORMATION"]') # Instances of reusable transformation objects
    global targetInstances
    targetInstances = mappingElement.findall('INSTANCE[@TYPE="TARGET"]')            # Instances of reusable target objects
    global connectors
    connectors = mappingElement.findall('CONNECTOR')                                # Mapping specific

    for sourceInstance in sourceInstances:
        processObjectDefinition(sourceInstance)


def processObjectDefinition(instance):
    """ Process Source Object definition based on the supplied "instance". \n
    Also, if flag "forceUpdateTarget" is true, process all Target Object definitions.
    Takes: XMLElement instance
    """
    global isTargetDef
    global sources
    global targets
    global sourceFields
    global targetFields
    global forceUpdateTarget
    global fieldMap
    # Find intance definition
    objectDefName = instance.attrib.get('TRANSFORMATION_NAME')
    for source in sources:
        if source.attrib.get('NAME') == objectDefName:
            targetFields = None
            sourceFields = source.findall('SOURCEFIELD')
            if sourceFields:
                for field in sourceFields:
                    fieldArray = fieldMap.get(field.attrib.get('NAME'))
                    if fieldArray:
                        fieldArray[0] = fieldArray[1]
                        print(6*"\t",fieldArray)
                        if DEBUG_ON: print("Processing:")
                        if DEBUG_ON: print(field)
                        processAlterFields(source, instance, field, fieldArray)
            break
    # If forceUpdateTarget is true, find the appropriate Target Object definition and force update the precision/decimal places there as well
    if forceUpdateTarget:
        for target in targets:
            if target.attrib.get('NAME') == objectDefName:
                sourceFields = None
                targetFields = target.findall('TARGETFIELD')
                if targetFields:
                    for field in targetFields:
                        fieldArray = fieldMap.get(field.attrib.get('NAME'))
                        if fieldArray:
                            fieldArray[0] = fieldArray[1]
                            print(6*"\t",fieldArray)
                            if DEBUG_ON: print("Processing:")
                            if DEBUG_ON: print(field)
                            processAlterFields(target, instance, field, fieldArray)
                break
    

# fieldArray[0] => SOURCE
# fieldArray[1] => TARGET
# fieldArray[2] => Original Type
# fieldArray[3] => New Type
# fieldArray[4] => Original Precision
# fieldArray[5] => New Precision
# fieldArray[6] => Original DecimalPos
# fieldArray[7] => New DecimalPos
# fieldArray[8] => Flag Attribute
# <CONNECTOR FROMFIELD ="UPG_EE_TMP2" FROMINSTANCE ="SQTRANS" FROMINSTANCETYPE ="Source Qualifier" TOFIELD ="UPG_EE_TMP2" TOINSTANCE ="exp_TestInstance" TOINSTANCETYPE ="Expression"/>
def processAlterFields(objectDef, instance, field, fieldArray):
    """ Recursive function.
    Process supplied object definition, requested instance of the object and supplied field which should be updated.
    The field is then recursively updated throughout the mapping, until it hits its last connetion (transformation), or until it reaches Target Object.

    Takes: XMLElement TargetObject, XMLElement TargetObjectInstance, XMLElement Field, Array FieldArrayFromCSV
    """
    global connectors
    global targets
    global targetInstances
    global transformInstances
    global transformations
    global reuseTransforms
    global DEBUG_ON
    global listOfChangedFields
    objectDefOut = objectDef
    instanceOut = instance
    fieldOut = field
    fieldArrayOut = fieldArray
    connectorArray = []
    connectorFound = False

    fieldName = field.attrib.get('NAME')
    fieldType = field.attrib.get('DATATYPE')
    fieldPrecision = field.attrib.get('PRECISION')
    fieldScale = field.attrib.get('SCALE')
    elementType = field.tag
    if DEBUG_ON: print("Processing:")
    if DEBUG_ON: print(elementType)
    if DEBUG_ON: print(fieldName)
    if DEBUG_ON: print(fieldType)
    
    # Check if field type is same:
    if fieldArray[2] != fieldArray[3]:
        #fieldType
        print("CHANGETYPE")
    #print("Current:"+fieldPrecision)
    #print("Original:"+fieldArray[4])
    #print("New:"+fieldArray[5])
    
    #
    # SET PRECISION
    #
    if int(fieldPrecision) < int(fieldArray[5]):
        print(8*"\t","Changing precision... Was:", fieldPrecision, "; New:",fieldArray[5])
        if listOfChangedFields.count(fieldArray[0]) == 0:
            listOfChangedFields.append(fieldArray[0])
        field.set('PRECISION', fieldArray[5])

    #
    # SET DECIMALS
    #
    if int(fieldScale) < int(fieldArray[7]):
        print(8*"\t","Changing decimal places... Was:", fieldScale, "; New:",fieldArray[7])
        field.set('SCALE', fieldArray[7])
    
    #
    # Check what is the next object to be changed
    #
    # If current element type is TARGETFIELD, do NOT continue processing - we have reached end of the chain
    if elementType != 'TARGETFIELD':
        for connector in connectors:
            connectorArray.clear()
            connectorFromField = connector.attrib.get('FROMFIELD')
            connectorFromInstance = connector.attrib.get('FROMINSTANCE')
            connectorFromInstanceType = connector.attrib.get('FROMINSTANCETYPE')
            connectorToField = connector.attrib.get('TOFIELD')
            connectorToInstance = connector.attrib.get('TOINSTANCE')
            connectorToInstanceType = connector.attrib.get('TOINSTANCETYPE')
            connectorArray.append(connectorFromField)
            connectorArray.append(connectorFromInstance)
            connectorArray.append(connectorToField)
            connectorArray.append(connectorToInstance)
            #print("CONNECTOR:")
            #print(7*"\t","CONNECTOR:",connectorArray)
            #print(connector.attrib)
            if connectorFromField == fieldName and connectorFromInstance == instance.attrib.get('NAME'):
                #print("CONNECTOR:")
                print(7*"\t","CONNECTOR:",connectorArray)
                if connector.attrib.get('TOINSTANCETYPE') == 'Target Definition':
                    # last field -> Target
                    for targetInstance in targetInstances:
                        if targetInstance.attrib.get('NAME') == connectorToInstance:

                            for target in targets:
                                if target.attrib.get('NAME') == targetInstance.attrib.get('TRANSFORMATION_NAME'):
                                    if DEBUG_ON: print(0)
                                    objectDefOut = target
                                    instanceOut = targetInstance
                                    fieldOut = target.findall('TARGETFIELD[@NAME="'+connectorToField+'"]')[0]
                                    break
                            break
                
                else:
                    for transformInstance in transformInstances:
                        if transformInstance.attrib.get('NAME') == connectorToInstance:
                            if transformInstance.attrib.get('REUSABLE') == 'YES':
                                for reuseTransform in reuseTransforms:
                                    if reuseTransform.attrib.get('NAME') == transformInstance.attrib.get('TRANSFORMATION_NAME'):
                                        if DEBUG_ON: print(1)
                                        objectDefOut = reuseTransform
                                        instanceOut = transformInstance
                                        fieldOut = reuseTransform.findall('TRANSFORMFIELD[@NAME="'+connectorToField+'"]')[0]
                                        break
                            else:
                                for transform in transformations:
                                    if (transform.attrib.get('NAME') == transformInstance.attrib.get('TRANSFORMATION_NAME') 
                                    and transform.findall('TRANSFORMFIELD[@NAME="'+connectorToField+'"]')
                                    and (connectorFromField == connectorToField)):
                                        objectDefOut = transform
                                        instanceOut = transformInstance
                                        if DEBUG_ON: print(2)
                                        #print(transform.findall('TRANSFORMFIELD[@REF_FIELD="'+connectorToField+'"]'))
                                        fieldOut = transform.findall('TRANSFORMFIELD[@NAME="'+connectorToField+'"]')[0]
                                        break
                                    elif (transform.attrib.get('NAME') == transformInstance.attrib.get('TRANSFORMATION_NAME') 
                                    and transform.findall('TRANSFORMFIELD[@NAME="'+connectorToField+'"]') and fieldArrayOut[1] == connectorToField ):
                                        objectDefOut = transform
                                        instanceOut = transformInstance
                                        if DEBUG_ON: print(3)
                                        if DEBUG_ON: print(transform.findall('TRANSFORMFIELD[@REF_FIELD="'+connectorToField+'"]'))
                                        fieldOut = transform.findall('TRANSFORMFIELD[@NAME="'+connectorToField+'"]')[0]
                                        break
                                    elif connectorFromField != connectorToField and connectorToInstanceType == "Router":
                                        for routerOutput in transform.findall('TRANSFORMFIELD[@REF_FIELD="'+connectorToField+'"]'):
                                            objectDefOut = transform
                                            instanceOut = transformInstance
                                            if DEBUG_ON: print(4)
                                            if DEBUG_ON: print(transform.findall('TRANSFORMFIELD[@REF_FIELD="'+connectorToField+'"]'))
                                            if DEBUG_ON: print(transform.findall('TRANSFORMFIELD[@NAME="'+routerOutput.attrib.get('NAME')+'"]'))
                                            fieldOut = routerOutput
                                            #processAlterFields(objectDefOut, instanceOut, fieldOut, fieldArrayOut)
                                            break
                                    elif connectorFromField != connectorToField  and connectorFromInstanceType == "Router":
                                        for routerOutput in transform.findall('TRANSFORMFIELD[@REF_FIELD="'+connectorToField+'"]'):
                                            objectDefOut = transform
                                            instanceOut = transformInstance
                                            if DEBUG_ON: print(5)
                                            if DEBUG_ON: print(transform.findall('TRANSFORMFIELD[@REF_FIELD="'+connectorToField+'"]'))
                                            if DEBUG_ON: print(transform.findall('TRANSFORMFIELD[@NAME="'+routerOutput.attrib.get('NAME')+'"]'))
                                            fieldOut = routerOutput
                                            #processAlterFields(objectDefOut, instanceOut, fieldOut, fieldArrayOut)
                                            break
                                    else:
                                        continue
                                        print("++++++++++++++++++NO!+++++++++++++++++++")
                                        print(transform.attrib.get('NAME'))
                                        print(transformInstance.attrib.get('TRANSFORMATION_NAME')) 
                                        print(transform.findall('TRANSFORMFIELD[@NAME="'+connectorToField+'"]'))
                                        print(connectorFromField)
                                        print(connectorToField)
                                        print("------------------NO!-------------------")
                                        
                                    
                            break
                connectorFound = True
                break
    #fieldPrecision = field.attrib.get('PRECISION')
    #print("Current:"+fieldPrecision)
    #print("Original:"+fieldArray[4])
    #print("New:"+fieldArray[5])
        if DEBUG_ON: print("Next:")
        if DEBUG_ON: print(fieldOut.tag)
        if DEBUG_ON: print(fieldOut.attrib.get('NAME'))
        if DEBUG_ON: print(fieldOut.attrib.get('TYPE'))
        if DEBUG_ON: print(connectorArray)
        if not connectorFound:
            Exception("CANNOT FIND NEXT CONNECTION!!!")
        else:
            processAlterFields(objectDefOut, instanceOut, fieldOut, fieldArrayOut)





#######################
##   MAIN PROCESS    ##
#######################
processInput()
