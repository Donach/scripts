#!/bin/bash
#   This script takes input file and processes each line, creating groups and assigning users into it.
#   Created by: Daniel Havlicek (Infa Partner)
#   First version: 30.06.2021
#   Changelog:  
#

if [[ "$1" == "" || "$2" == "" || ! "$2" =~ "/" ]]; then
    echo "ERROR: One or more input parameters is empty!"
    echo "HOWTO: run this script with following parameters:"
    echo "1: DOMAIN_NAME - specify domain name to be used for creating groups"
    echo "2: INPUT_FILE - csv file with all data using following header: \"GROUPNAME\",\"USERID\",\"DESCRIPTION\",\"SECURITYDOMAIN\""
    echo "INPUT_FILE should be specified as full path to given file."
    echo "Valid values for GROUPNAME: DI_MICHA_00_LTEST_00_Execute"
    echo "Valid values for USERID: LSAQU88DR"
    echo "Valid values for DESCRIPTION: <full name>"
    echo "Valid values for SECURITYDOMAIN: ADMIN_CZ"
    echo "Terminating..."
    exit 1
fi

function log () {
    echo "[`date '+%F %H:%M:%S'`] $1"
}

function logDev () {
    if [[ "$isDev" == "true" ]]; then
        log $1
    fi
}

# This function will run infacmd.sh command with specified parameters
# Parameters: $1=indexOfRow, $2=commandParamaters
function f_infacmdCustom () {
    if [[ "$2" != "" ]]; then
        infaCmdFull="infacmd.sh $2" # AssignGroupPermission -dn $2 -on $5 -ot $4 -eg $3

        #logDev "Running infacmd command:"
        #logDev "Command: $infaCmdFull"

        cmdResultCode=0
        cmdResult=$(eval $infaCmdFull; cmdResultCode=$?;)
        
        #logDev "Command Result: $cmdResult"
        #logDev "Command Result Code: $cmdResultCode"

        if [[ "$cmdResultCode" == "0" && ! "$cmdResult" =~ "Type help:" && ! "$cmdResult" =~ "failed with error" ]]; then
            #logDev "Command infacmd finished successfully! Row: $1, Parameters: $2, ResultCode: $cmdResultCode" #Row: $1, ObjectType: $4, Object: $5, Group: $3
            return 0
        else
            log "ERROR: Command infacmd failed! Row: $1, Parameters: $2, ResultCode: $cmdResultCode"
            log "Result: $cmdResult"
            return 1
        fi
    fi
    return 2
}

DOMAIN_NAME=$1
INPUT_FILE=$2
ProcessedLinesFile="$INPUT_FILE.processed"
SkippedLinesFile="$INPUT_FILE.skipped"
LOG_FILE="$INPUT_FILE_`date +%F`.log"
EMPTY_STRING="NULL"

echo "Logging all output to file $LOG_FILE"
exec &>> $LOG_FILE
isDev=$( if [[ "$DOMAIN_NAME" == "devdom_eng" ]]; then echo "true"; else echo ""; fi )

# Create arrays from parsing the input file
arrGroupName=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ',' -f1)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ',' -f1 | sed 's/"//g')"; fi; done ) )
arrUserID=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ',' -f2)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ',' -f2 | sed 's/"//g')"; fi; done ) )
arrDescription=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ',' -f3)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ',' -f3 | sed 's/"//g')"; fi; done ) )
arrSecDomain=( $( cat $INPUT_FILE | while read line; do if [[ "$(echo "$line" | cut -d ',' -f4)" == "" ]]; then echo "$EMPTY_STRING"; else echo "$(echo "$line" | cut -d ',' -f4 | sed 's/"//g')"; fi; done ) )


log "Process started..."

log "Creating log files for processed/skipped files:"
logDev "$LOG_FILE"
logDev "$ProcessedLinesFile"
logDev "$SkippedLinesFile"

echo "" > $ProcessedLinesFile
echo "" > $SkippedLinesFile

if [[ ! -f "$ProcessedLinesFile" || ! -f "$SkippedLinesFile" || ! -f "$LOG_FILE" ]]; then
    log "ERROR: Cannot create log files!"
    log "Terminating..."
    exit 1
fi

listOfAllGroups=$( infacmd.sh listAllGroups -dn $DOMAIN_NAME | head -n -1 )

# Do for all array elements
for (( index = 1; index < ${#arrGroupName[@]}; index++ ))
do 
    groupName="${arrGroupName[$index]}"
    userID="${arrUserID[$index]}"
    description="${arrDescription[$index]}"
    secDomain="${arrSecDomain[$index]}"

    fullLine="\"$groupName\",\"$userID\",\"$description\",\"$secDomain\""

    # If group is empty, skip this row
    if [[ "$groupName" == "$EMPTY_STRING" || "$groupName" =~ "GROUPNAME" ]]; then
        echo "$fullLine" >> $SkippedLinesFile
        continue
    fi

    # Check if group was already created, if not create it
    if [[ ! "$listOfAllGroups" =~ "$groupName" ]]; then
        f_infacmdCustom "$index" "CreateGroup -dn $DOMAIN_NAME -sdn Native -gn '$groupName'" # infacmd.sh CreateGroup -dn devdom_eng -sdn Native -gn DI_EXECUTE/DI_ALFAX_00_FACTX_00_Execute
        listOfAllGroups="$listOfAllGroups;$groupName"
    fi

    # AssignUserToGroup 
    if [[ "$userID" != "$EMPTY_STRING" ]]; then
        addSecDom=$( if [[ "$secDomain" != "$EMPTY_STRING" ]]; then echo "-esd '$secDomain'"; else echo ""; fi )
        f_infacmdCustom "$index" "AddUserToGroup -dn $DOMAIN_NAME -gn '$groupName' -eu '$userID' $addSecDom" # infacmd.sh AddUserToGroup -dn devdom_eng -gn DI_MICHA_00_LTEST_00 -eu LSAQU88DR -sdn ADMIN_CZ
        cmdResult=$?
        if [[ "$cmdResult" != "0" ]]; then
            log "Skipping row $index"
            echo "$fullLine" >> $SkippedLinesFile
            continue
        fi
    fi

    echo "$fullLine" >> $ProcessedLinesFile

done

log "All rows were processed!"
log "Terminating..."
